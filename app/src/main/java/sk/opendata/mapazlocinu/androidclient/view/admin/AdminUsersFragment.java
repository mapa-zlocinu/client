/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.view.admin;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.adapters.AdminUsersAdapter;
import sk.opendata.mapazlocinu.androidclient.api.ApiAdminController;
import sk.opendata.mapazlocinu.androidclient.api.listeners.admin.AdminUsersListener;
import sk.opendata.mapazlocinu.androidclient.dialogs.AdminUserDialog;
import sk.opendata.mapazlocinu.androidclient.dividers.ItemDecorationDivider;
import sk.opendata.mapazlocinu.androidclient.model.user.UserAdmin;
import sk.opendata.mapazlocinu.androidclient.model.user.UserAdminUpdate;
import sk.opendata.mapazlocinu.androidclient.utils.Utils;

public class AdminUsersFragment extends Fragment implements AdminUsersListener, AdminUserDialog.UserDialogListener {
    private SwipeRefreshLayout swipeRefreshLayout;
    private AdminUsersAdapter adapter;
    private ProgressDialog progressDialog;

    public AdminUsersFragment() {
    }

    public static Fragment newInstance() {
        return new AdminUsersFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin_users, container, false);

        progressDialog = new ProgressDialog(getActivity());
        Utils.openProgressDialog(progressDialog, getActivity());

        initView(view);
        loadUsersData();

        return view;
    }

    private void initView(View view) {
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.users_admin_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new AdminUsersAdapter(new ArrayList<UserAdmin>(), getActivity(), this);

        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new ItemDecorationDivider(getActivity()));

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.users_admin_recycler_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadUsersData();
            }
        });
    }

    private void loadUsersData() {
        ApiAdminController.getInstance().registerListener(this);
        ApiAdminController.getInstance().getUsersList(getActivity());
    }

    private void stopRefreshLayout() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    public void openUsersDialog(UserAdmin userAdmin) {
        AdminUserDialog dialog = AdminUserDialog.newInstance(userAdmin);
        dialog.setTargetFragment(this, 250);
        dialog.show(getChildFragmentManager(), "AdminUserDialog");
    }

    @Override
    public void onUsersListLoad(List<UserAdmin> users) {
        if (users == null || users.isEmpty()) {
            Toast.makeText(getActivity(), R.string.no_data, Toast.LENGTH_LONG).show();
        }

        adapter.addNewDataSet(users);

        stopRefreshLayout();
        Utils.closeProgressDialog(progressDialog);
    }

    @Override
    public void onUpdateCompleted(UserAdmin userAdmin) {
        Toast.makeText(getActivity(), R.string.operation_done, Toast.LENGTH_LONG).show();
        adapter.updateUser(userAdmin);
        Utils.closeProgressDialog(progressDialog);
    }

    @Override
    public void onOperationError() {
        Toast.makeText(getActivity(), R.string.load_error, Toast.LENGTH_LONG).show();
        stopRefreshLayout();
        Utils.closeProgressDialog(progressDialog);
    }

    @Override
    public void onUserUpdateDialogSubmit(String userEmail, String username, String rating) {
        boolean isError = false;
        int ratingValue = 0;

        if (username.length() < 3 || username.length() > 100) {
            isError = true;
        }
        try {
            ratingValue = Integer.valueOf(rating);
        } catch (NumberFormatException e) {
            isError = true;
        }

        if (isError) {
            Toast.makeText(getActivity(), R.string.validation_error, Toast.LENGTH_LONG).show();
        } else {
            UserAdminUpdate update = new UserAdminUpdate();
            update.setRating(ratingValue);
            update.setName(username);

            Utils.openProgressDialog(progressDialog, getActivity());
            ApiAdminController.getInstance().updateUser(userEmail, update, getActivity());
        }
    }
}
