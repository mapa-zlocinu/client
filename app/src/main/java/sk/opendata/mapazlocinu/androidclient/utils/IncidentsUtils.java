/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.utils;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.model.Link;
import sk.opendata.mapazlocinu.androidclient.model.attachment.AttachmentAdmin;
import sk.opendata.mapazlocinu.androidclient.model.attachment.AttachmentPublic;
import sk.opendata.mapazlocinu.androidclient.model.enums.CommentType;
import sk.opendata.mapazlocinu.androidclient.model.enums.IncidentType;
import sk.opendata.mapazlocinu.androidclient.model.enums.Visibility;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentAdmin;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentPublic;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentPublicExcerpt;

public class IncidentsUtils {
    private static final SimpleDateFormat DATE_AND_TIME_FORMAT = new SimpleDateFormat("dd.MM.yyyy, HH:mm");
    private static final DecimalFormat df = new DecimalFormat("0.00");
    private static final String ATTACHMENT_DATA_REL = "attachment-data";

    public static LatLng incidentExcerptToLatLng(IncidentPublicExcerpt incident) {
        return new LatLng(incident.getLat(), incident.getLon());
    }

    public static LatLng incidentExcerptToLatLng(IncidentPublic incident) {
        return new LatLng(incident.getLat(), incident.getLon());
    }

    public static LatLng incidentExcerptToLatLng(IncidentAdmin incident) {
        return new LatLng(incident.getLat(), incident.getLon());
    }

    public static BitmapDescriptor incidentTypeToMarkerColor(IncidentPublicExcerpt incident) {
        return incidentTypeToMarkerColor(incident.getType());
    }

    public static BitmapDescriptor incidentTypeToMarkerColor(IncidentType incidentType) {
        if (incidentType == null) {
            return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE);
        }

        float colorType;

        switch (incidentType) {
            case ECONOMIC:
                colorType = BitmapDescriptorFactory.HUE_AZURE;
                break;
            case MORAL:
                colorType = BitmapDescriptorFactory.HUE_CYAN;
                break;
            case ORGANIZED:
                colorType = BitmapDescriptorFactory.HUE_GREEN;
                break;
            case PROPERTY:
                colorType = BitmapDescriptorFactory.HUE_ORANGE;
                break;
            case VIOLENT:
                colorType = BitmapDescriptorFactory.HUE_RED;
                break;
            default:
                colorType = BitmapDescriptorFactory.HUE_BLUE;
                break;
        }

        return BitmapDescriptorFactory.defaultMarker(colorType);
    }

    public static int visibilityToStringResource(Visibility visibility) {
        if (visibility == null) {
            return R.string.visibility_pending;
        }

        int resource;

        switch (visibility) {
            case APPROVED:
                resource = R.string.visibility_approved;
                break;
            case DISAPPROVED:
                resource = R.string.visibility_disapproved;
                break;
            default:
                resource = R.string.visibility_pending;
                break;
        }

        return resource;
    }

    public static int incidentTypeToStringResource(IncidentType incidentType) {
        if (incidentType == null) {
            return R.string.incident_type_other;
        }

        int typeText;

        switch (incidentType) {
            case ECONOMIC:
                typeText = R.string.incident_type_economic;
                break;
            case MORAL:
                typeText = R.string.incident_type_moral;
                break;
            case ORGANIZED:
                typeText = R.string.incident_type_organized;
                break;
            case PROPERTY:
                typeText = R.string.incident_type_property;
                break;
            case VIOLENT:
                typeText = R.string.incident_type_violent;
                break;
            default:
                typeText = R.string.incident_type_other;
                break;
        }

        return typeText;
    }

    public static int incidentTypeToStringResource(IncidentPublicExcerpt incident) {
        return incidentTypeToStringResource(incident.getType());
    }

    public static String toFormattedDate(long incidentDate) {
        Date date = new Date(incidentDate);
        return DATE_AND_TIME_FORMAT.format(date);
    }

    public static int commentTypeToStringResource(CommentType commentType) {
        if (commentType == null) {
            return R.string.comment_type_neutral;
        }

        int typeText;

        switch (commentType) {
            case CONFIRMATION:
                typeText = R.string.comment_type_confirmation;
                break;
            case REFUTATION:
                typeText = R.string.comment_type_refutation;
                break;
            default:
                typeText = R.string.comment_type_neutral;
                break;
        }

        return typeText;
    }

    public static String getAttachmentDataLink(AttachmentPublic attachment) {
        return getAttachmentDataLink(attachment.getLinks());
    }

    public static String getAttachmentDataLink(AttachmentAdmin attachment) {
        return getAttachmentDataLink(attachment.getLinks());
    }

    private static String getAttachmentDataLink(List<Link> links) {
        if (links == null) {
            return null;
        }

        for (Link link : links) {
            if (link.getRel().equals(ATTACHMENT_DATA_REL)) {
                return link.getHref();
            }
        }

        return null;
    }

    public static String roundNumberToMB(long number) {
        double tmp = (double) (number / 1024) / 1024;
        return df.format(tmp) + " MB";
    }

    public static int incidentTypeToSpinnerIndex(IncidentType type) {
        if (type.equals(IncidentType.PROPERTY))
            return 0;
        else if (type.equals(IncidentType.ECONOMIC))
            return 1;
        else if (type.equals(IncidentType.VIOLENT))
            return 2;
        else if (type.equals(IncidentType.MORAL))
            return 3;
        else if (type.equals(IncidentType.ORGANIZED))
            return 4;
        else
            return 5;
    }

    public static IncidentType spinnerIndexToIncidentType(int index) {
        if (index == 0)
            return IncidentType.PROPERTY;
        else if (index == 1)
            return IncidentType.ECONOMIC;
        else if (index == 2)
            return IncidentType.VIOLENT;
        else if (index == 3)
            return IncidentType.MORAL;
        else if (index == 4)
            return IncidentType.ORGANIZED;
        else
            return IncidentType.OTHER;
    }
}
