/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.model.user.UserAdmin;
import sk.opendata.mapazlocinu.androidclient.view.admin.AdminUsersFragment;

public class AdminUsersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    final private Context context;
    final private AdminUsersFragment fragment;
    private List<UserAdmin> users;

    public AdminUsersAdapter(List<UserAdmin> users, Context context, AdminUsersFragment fragment) {
        this.users = users != null ? users : new ArrayList<UserAdmin>();
        this.context = context;
        this.fragment = fragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.users_admin_list_item, parent, false);
        return new UserItemHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        UserItemHolder itemHolder = (UserItemHolder) holder;

        final UserAdmin item = this.users.get(position);
        itemHolder.nameTextView.setText(item.getName());
        itemHolder.emailTextView.setText(item.getEmail());
        itemHolder.ratingTextView.setText(context.getString(R.string.user_rating_placeholder, item.getRating()));

        itemHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment.openUsersDialog(item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.users.size();
    }

    public void addNewDataSet(List<UserAdmin> users) {
        this.users = users != null ? users : new ArrayList<UserAdmin>();
        notifyDataSetChanged();
    }

    public void updateUser(UserAdmin userAdmin) {
        if (userAdmin == null) return;

        this.users.remove(userAdmin);
        this.users.add(userAdmin);
        notifyDataSetChanged();
    }

    private class UserItemHolder extends RecyclerView.ViewHolder {
        final View view;
        final TextView nameTextView;
        final TextView emailTextView;
        final TextView ratingTextView;

        UserItemHolder(View itemView) {
            super(itemView);

            this.view = itemView;
            this.nameTextView = (TextView) view.findViewById(R.id.user_name);
            this.emailTextView = (TextView) view.findViewById(R.id.user_email);
            this.ratingTextView = (TextView) view.findViewById(R.id.user_rating);
        }
    }
}
