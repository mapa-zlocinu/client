/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.view.detail;


import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import sk.opendata.mapazlocinu.androidclient.PublicIncidentDetailActivity;
import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.adapters.PublicCommentsListAdapter;
import sk.opendata.mapazlocinu.androidclient.api.ApiPublicController;
import sk.opendata.mapazlocinu.androidclient.api.listeners.PublicCommentsLoadListener;
import sk.opendata.mapazlocinu.androidclient.dividers.ItemDecorationDivider;
import sk.opendata.mapazlocinu.androidclient.model.comment.CommentPublic;

public class PublicIncidentCommentsFragment extends Fragment implements PublicCommentsLoadListener {
    private SwipeRefreshLayout swipeRefreshLayout;
    private PublicCommentsListAdapter adapter;

    public PublicIncidentCommentsFragment() {
    }

    public static PublicIncidentCommentsFragment newInstance() {
        return new PublicIncidentCommentsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_public_incident_comments, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.comments_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new PublicCommentsListAdapter(new ArrayList<CommentPublic>(), getActivity());

        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new ItemDecorationDivider(getActivity()));

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.comments_recycler_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadComments();
            }
        });

        loadComments();

        return view;
    }

    private void loadComments() {
        ApiPublicController.getInstance().registerListener(this);
        ApiPublicController.getInstance().loadPublicIncidentsComments(((PublicIncidentDetailActivity) getActivity()).getIncidentId());
    }

    @Override
    public void onIncidentCommentsLoad(List<CommentPublic> comments) {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }

        adapter.addNewDataSet(comments);
    }
}
