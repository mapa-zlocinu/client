/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.view.main;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.maps.android.heatmaps.HeatmapTileProvider;

import java.util.ArrayList;
import java.util.List;

import sk.opendata.mapazlocinu.androidclient.PublicIncidentDetailActivity;
import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.data.MainActivityDataHolder;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentPublicExcerpt;
import sk.opendata.mapazlocinu.androidclient.utils.IncidentsUtils;


public class MainActivityMapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {
    public static final String MAP_TYPE_NORMAL = "map.normal";
    public static final String MAP_TYPE_HEAT = "map.heat";
    private static final String MAP_TYPE_PARAM = "map.type";

    private GoogleMap map;
    private String currentMapType;

    public MainActivityMapFragment() {
    }

    public static Fragment newInstance(String param2) {
        MainActivityMapFragment fragment = new MainActivityMapFragment();

        Bundle args = new Bundle();
        args.putString(MAP_TYPE_PARAM, param2);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            currentMapType = getArguments().getString(MAP_TYPE_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_activity_map, container, false);

        MapFragment mapFragment;
        mapFragment = (MapFragment) getChildFragmentManager().findFragmentById(R.id.incidents_list_map);
        mapFragment.getMapAsync(this);

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        initMap();
    }

    private void initMap() {
        List<IncidentPublicExcerpt> incidents = MainActivityDataHolder.getInstance().getPublicIncidents();
        if (incidents == null || incidents.size() == 0) {
            Toast.makeText(getActivity(), R.string.no_data, Toast.LENGTH_SHORT).show();
            return;
        }

        if (currentMapType != null && currentMapType.equals(MAP_TYPE_HEAT)) {
            addHeatLayer();
        } else {
            addMarkerLayer();
        }
    }

    private void addMarkerLayer() {
        List<IncidentPublicExcerpt> incidents = MainActivityDataHolder.getInstance().getPublicIncidents();

        LatLngBounds.Builder boundBuilders = LatLngBounds.builder();
        for (IncidentPublicExcerpt incident : incidents) {
            LatLng latLng = IncidentsUtils.incidentExcerptToLatLng(incident);
            boundBuilders = boundBuilders.include(latLng);

            MarkerOptions options = new MarkerOptions().position(latLng)
                    .title(incident.getTitle())
                    .snippet(getString(IncidentsUtils.incidentTypeToStringResource(incident)))
                    .icon(IncidentsUtils.incidentTypeToMarkerColor(incident));

            Marker marker = this.map.addMarker(options);
            marker.setTag(incident.getId());
        }

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;

        this.map.getUiSettings().setMapToolbarEnabled(false);
        this.map.moveCamera(CameraUpdateFactory.newLatLngBounds(boundBuilders.build(), width, height, 100));
        this.map.setOnInfoWindowClickListener(this);
    }

    private void addHeatLayer() {
        List<IncidentPublicExcerpt> incidents = MainActivityDataHolder.getInstance().getPublicIncidents();

        List<LatLng> latLngs = new ArrayList<>();
        LatLngBounds.Builder boundBuilders = LatLngBounds.builder();

        for (IncidentPublicExcerpt incident : incidents) {
            LatLng latLng = IncidentsUtils.incidentExcerptToLatLng(incident);
            boundBuilders = boundBuilders.include(latLng);
            latLngs.add(latLng);
        }

        HeatmapTileProvider heatmapTileProvider = new HeatmapTileProvider.Builder()
                .data(latLngs)
                .build();

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;

        this.map.addTileOverlay(new TileOverlayOptions().tileProvider(heatmapTileProvider));
        this.map.moveCamera(CameraUpdateFactory.newLatLngBounds(boundBuilders.build(), width, height, 100));
    }

    public void refreshData() {
        this.map.clear();
        initMap();
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Intent intent = new Intent(getActivity(), PublicIncidentDetailActivity.class);
        intent.putExtra(PublicIncidentDetailActivity.INCIDENT_ID_ARG, (String) marker.getTag());
        getActivity().startActivity(intent);
    }
}
