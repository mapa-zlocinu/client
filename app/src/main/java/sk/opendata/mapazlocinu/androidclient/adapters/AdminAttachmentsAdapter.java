/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.ceylonlabs.imageviewpopup.ImagePopup;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Authenticator;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.model.attachment.AttachmentAdmin;
import sk.opendata.mapazlocinu.androidclient.utils.AuthUtils;
import sk.opendata.mapazlocinu.androidclient.utils.IncidentsUtils;
import sk.opendata.mapazlocinu.androidclient.view.admindetail.AdminIncidentAttachmentFragment;

public class AdminAttachmentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Picasso picasso;
    private final ImagePopup imagePopup;
    private final AdminIncidentAttachmentFragment fragment;
    private List<AttachmentAdmin> attachments;

    public AdminAttachmentsAdapter(List<AttachmentAdmin> attachments, final Context context, AdminIncidentAttachmentFragment fragment) {
        this.attachments = attachments != null ? attachments : new ArrayList<AttachmentAdmin>();
        this.fragment = fragment;

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .authenticator(new Authenticator() {
                    @Override
                    public Request authenticate(Route route, Response response) throws IOException {
                        return response.request().newBuilder()
                                .header("Authorization", AuthUtils.getAuthTokenWithPrefix(context))
                                .build();
                    }
                })
                .build();

        this.picasso = new Picasso.Builder(context)
                .downloader(new OkHttp3Downloader(okHttpClient))
                .build();

        this.imagePopup = new ImagePopup(context);
        this.imagePopup.setBackgroundColor(Color.WHITE);
        this.imagePopup.setWindowWidth(900);
        this.imagePopup.setWindowHeight(900);
        this.imagePopup.setHideCloseIcon(true);
        this.imagePopup.setImageOnClickClose(true);
        this.imagePopup.setAnimation(new AlphaAnimation(0, 1));
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.attachment_admin_list_item, parent, false);
        return new AttachmentItemHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final AttachmentItemHolder itemHolder = (AttachmentItemHolder) holder;

        final AttachmentAdmin attachment = this.attachments.get(position);
        final String title = attachment.getTitle() + attachment.getExtension();

        itemHolder.attachmentTitle.setText(title);
        itemHolder.attachmentDate.setText(IncidentsUtils.toFormattedDate(attachment.getCreatedDate()));
        itemHolder.attachmentSize.setText(IncidentsUtils.roundNumberToMB(attachment.getSize()));

        try {
            picasso.load(IncidentsUtils.getAttachmentDataLink(attachment))
                    .resize(600, 600)
                    .centerInside()
                    .into(itemHolder.attachmentImageView);

            itemHolder.attachmentImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    imagePopup.initiatePopup(itemHolder.attachmentImageView.getDrawable());
                }
            });
        } catch (Exception e) {
            Log.e("AdminAttachmentsAdapter", "Error while setting up picasso.", e);
        }

        itemHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment.showAttachmentDialog(attachment.getId(), title);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.attachments.size();
    }

    public void addNewDataSet(List<AttachmentAdmin> attachments) {
        this.attachments = attachments != null ? attachments : new ArrayList<AttachmentAdmin>();
        notifyDataSetChanged();
    }

    private class AttachmentItemHolder extends RecyclerView.ViewHolder {
        final View view;
        final TextView attachmentTitle;
        final TextView attachmentDate;
        final TextView attachmentSize;
        final ImageView attachmentImageView;

        AttachmentItemHolder(View itemView) {
            super(itemView);

            this.view = itemView;
            this.attachmentTitle = (TextView) view.findViewById(R.id.attachment_title);
            this.attachmentDate = (TextView) view.findViewById(R.id.attachment_date);
            this.attachmentSize = (TextView) view.findViewById(R.id.attachment_size);
            this.attachmentImageView = (ImageView) view.findViewById(R.id.attachment_image_view);
        }
    }
}
