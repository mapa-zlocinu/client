/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.view.detail;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import sk.opendata.mapazlocinu.androidclient.PublicIncidentDetailActivity;
import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.api.ApiPublicController;
import sk.opendata.mapazlocinu.androidclient.api.listeners.PublicIncidentDataLoadListener;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentPublic;
import sk.opendata.mapazlocinu.androidclient.utils.IncidentsUtils;

public class PublicIncidentDataFragment extends Fragment implements PublicIncidentDataLoadListener, OnMapReadyCallback {
    private View view;

    private GoogleMap map;
    private IncidentPublic incident;

    public PublicIncidentDataFragment() {
    }

    public static PublicIncidentDataFragment newInstance() {
        return new PublicIncidentDataFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_public_incident_data, container, false);

        MapFragment mapFragment;
        mapFragment = (MapFragment) getChildFragmentManager().findFragmentById(R.id.incident_map);
        mapFragment.getMapAsync(this);

        ApiPublicController.getInstance().registerListener(this);
        ApiPublicController.getInstance().loadPublicIncidentData(((PublicIncidentDetailActivity) getActivity()).getIncidentId());

        return view;
    }

    @Override
    public void onPublicIncidentDataLoad(IncidentPublic incident) {
        if (incident == null) {
            return;
        }
        this.incident = incident;

        ((TextView) view.findViewById(R.id.incident_title)).setText(incident.getTitle());
        ((TextView) view.findViewById(R.id.incident_description)).setText(incident.getDescription());

        ((TextView) view.findViewById(R.id.incident_date).findViewById(R.id.text_key)).setText(R.string.incident_date);
        ((TextView) view.findViewById(R.id.incident_date).findViewById(R.id.text_value)).setText(IncidentsUtils.toFormattedDate(incident.getIncidentDate()));

        ((TextView) view.findViewById(R.id.incident_created_date).findViewById(R.id.text_key)).setText(R.string.creation_date);
        ((TextView) view.findViewById(R.id.incident_created_date).findViewById(R.id.text_value)).setText(IncidentsUtils.toFormattedDate(incident.getCreatedDate()));

        ((TextView) view.findViewById(R.id.incident_type).findViewById(R.id.text_key)).setText(R.string.incident_type);
        ((TextView) view.findViewById(R.id.incident_type).findViewById(R.id.text_value)).setText(IncidentsUtils.incidentTypeToStringResource(incident.getType()));

        ((TextView) view.findViewById(R.id.incident_author).findViewById(R.id.text_key)).setText(R.string.author);
        if (incident.isAuthorVisible() && incident.getAuthor() != null) {
            String author = incident.getAuthor().getName() + " (" + incident.getAuthor().getEmail() + ")";
            ((TextView) view.findViewById(R.id.incident_author).findViewById(R.id.text_value)).setText(author);
        } else {
            ((TextView) view.findViewById(R.id.incident_author).findViewById(R.id.text_value)).setText(R.string.comment_anonymous);
        }

        ((TextView) view.findViewById(R.id.incident_address).findViewById(R.id.text_key)).setText(R.string.incident_address);
        ((TextView) view.findViewById(R.id.incident_address).findViewById(R.id.text_value)).setText(incident.getLocationAddress());

        initMap();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        initMap();
    }

    private void initMap() {
        if (this.map == null || this.incident == null) {
            return;
        }

        LatLng latLng = IncidentsUtils.incidentExcerptToLatLng(this.incident);
        MarkerOptions options = new MarkerOptions().position(latLng)
                .title(getString(R.string.incident_location))
                .icon(IncidentsUtils.incidentTypeToMarkerColor(this.incident.getType()));

        this.map.getUiSettings().setMapToolbarEnabled(false);
        this.map.getUiSettings().setAllGesturesEnabled(false);
        this.map.getUiSettings().setZoomControlsEnabled(true);
        this.map.addMarker(options);
        this.map.moveCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(latLng, 10)));
    }
}
