/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.api;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import sk.opendata.mapazlocinu.androidclient.api.config.ApiClient;
import sk.opendata.mapazlocinu.androidclient.api.interfaces.AdminApiInterface;
import sk.opendata.mapazlocinu.androidclient.api.listeners.admin.AdminCommentsListener;
import sk.opendata.mapazlocinu.androidclient.api.listeners.admin.AdminIncidentAttachmentsListener;
import sk.opendata.mapazlocinu.androidclient.api.listeners.admin.AdminIncidentCommentsListener;
import sk.opendata.mapazlocinu.androidclient.api.listeners.admin.AdminIncidentDataListener;
import sk.opendata.mapazlocinu.androidclient.api.listeners.admin.AdminIncidentDetailListener;
import sk.opendata.mapazlocinu.androidclient.api.listeners.admin.AdminIncidentUpdateListener;
import sk.opendata.mapazlocinu.androidclient.api.listeners.admin.AdminIncidentsListener;
import sk.opendata.mapazlocinu.androidclient.api.listeners.admin.AdminLoginListener;
import sk.opendata.mapazlocinu.androidclient.api.listeners.admin.AdminUsersListener;
import sk.opendata.mapazlocinu.androidclient.api.listeners.admin.DataSummaryLoadListener;
import sk.opendata.mapazlocinu.androidclient.model.admin.DataSummary;
import sk.opendata.mapazlocinu.androidclient.model.attachment.AttachmentAdmin;
import sk.opendata.mapazlocinu.androidclient.model.comment.CommentAdmin;
import sk.opendata.mapazlocinu.androidclient.model.comment.CommentUpdate;
import sk.opendata.mapazlocinu.androidclient.model.enums.Visibility;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentAdmin;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentAdminExcerpt;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentUpdate;
import sk.opendata.mapazlocinu.androidclient.model.security.LoginRequest;
import sk.opendata.mapazlocinu.androidclient.model.security.LoginResponse;
import sk.opendata.mapazlocinu.androidclient.model.user.UserAdmin;
import sk.opendata.mapazlocinu.androidclient.model.user.UserAdminUpdate;
import sk.opendata.mapazlocinu.androidclient.utils.AuthUtils;

public class ApiAdminController {
    private static final String TAG = "ApiAdminController";
    private static final int GET_MAX_SIZE = 3500;
    private static final String GET_SORT_TYPE = "desc";
    private static final String GET_SORT_BY = "date";

    private static ApiAdminController instance;
    private final AdminApiInterface service;

    private AdminLoginListener adminLoginListener;
    private DataSummaryLoadListener dataSummaryLoadListener;
    private AdminUsersListener adminUsersListener;
    private AdminCommentsListener adminCommentsListener;
    private AdminIncidentsListener adminIncidentsListener;
    private AdminIncidentDataListener adminIncidentDataListener;
    private AdminIncidentCommentsListener adminIncidentCommentsListener;
    private AdminIncidentAttachmentsListener adminIncidentAttachmentsListener;
    private AdminIncidentDetailListener adminIncidentDetailListener;
    private AdminIncidentUpdateListener adminIncidentUpdateListener;

    private ApiAdminController() {
        Retrofit retrofit = ApiClient.getInstance();
        service = retrofit.create(AdminApiInterface.class);
    }

    public static ApiAdminController getInstance() {
        if (instance == null) {
            instance = new ApiAdminController();
        }

        return instance;
    }

    public void registerListener(AdminLoginListener listener) {
        this.adminLoginListener = listener;
    }

    public void registerListener(DataSummaryLoadListener listener) {
        this.dataSummaryLoadListener = listener;
    }

    public void registerListener(AdminUsersListener listener) {
        this.adminUsersListener = listener;
    }

    public void registerListener(AdminCommentsListener listener) {
        this.adminCommentsListener = listener;
    }

    public void registerListener(AdminIncidentsListener listener) {
        this.adminIncidentsListener = listener;
    }

    public void registerListener(AdminIncidentDataListener listener) {
        this.adminIncidentDataListener = listener;
    }

    public void registerListener(AdminIncidentCommentsListener listener) {
        this.adminIncidentCommentsListener = listener;
    }

    public void registerListener(AdminIncidentAttachmentsListener listener) {
        this.adminIncidentAttachmentsListener = listener;
    }

    public void registerListener(AdminIncidentDetailListener listener) {
        this.adminIncidentDetailListener = listener;
    }

    public void registerListener(AdminIncidentUpdateListener listener) {
        this.adminIncidentUpdateListener = listener;
    }

    public void adminLogin(final Context context, final LoginRequest loginRequest) {
        Call<LoginResponse> call = service.postLoginRequest(loginRequest);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    AuthUtils.storeAuthDetails(context, response.body(), loginRequest);
                    adminLoginListener.onAdminLoginSuccess();
                } else {
                    adminLoginListener.onAdminLoginFailure();
                    logErrorBody(response);
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
                adminLoginListener.onAdminLoginFailure();
            }
        });
    }

    public void getDataSummary(Context context) {
        Call<DataSummary> call = service.getDataSummary(auth(context));
        call.enqueue(new Callback<DataSummary>() {
            @Override
            public void onResponse(Call<DataSummary> call, Response<DataSummary> response) {
                if (response.isSuccessful()) {
                    dataSummaryLoadListener.onSummaryLoadFinish(response.body());
                } else {
                    logErrorBody(response);
                    dataSummaryLoadListener.onSummaryLoadFinish(null);
                }
            }

            @Override
            public void onFailure(Call<DataSummary> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
                dataSummaryLoadListener.onSummaryLoadFinish(null);
            }
        });
    }

    public void getUsersList(Context context) {
        Call<List<UserAdmin>> call = service.getUsersList(auth(context), GET_MAX_SIZE, "name", GET_SORT_TYPE);
        call.enqueue(new Callback<List<UserAdmin>>() {
            @Override
            public void onResponse(Call<List<UserAdmin>> call, Response<List<UserAdmin>> response) {
                if (response.isSuccessful()) {
                    adminUsersListener.onUsersListLoad(response.body());
                } else {
                    logErrorBody(response);
                    adminUsersListener.onOperationError();
                }
            }

            @Override
            public void onFailure(Call<List<UserAdmin>> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
                adminUsersListener.onOperationError();
            }
        });
    }

    public void updateUser(String userEmail, UserAdminUpdate userAdminUpdate, Context context) {
        Call<UserAdmin> call = service.patchUser(auth(context), userEmail, userAdminUpdate);
        call.enqueue(new Callback<UserAdmin>() {
            @Override
            public void onResponse(Call<UserAdmin> call, Response<UserAdmin> response) {
                if (response.isSuccessful()) {
                    adminUsersListener.onUpdateCompleted(response.body());
                } else {
                    logErrorBody(response);
                    adminUsersListener.onOperationError();
                }
            }

            @Override
            public void onFailure(Call<UserAdmin> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
                adminUsersListener.onOperationError();
            }
        });
    }

    public void getComments(Context context, final Visibility visibility) {
        Call<List<CommentAdmin>> call;
        if (visibility.equals(Visibility.PENDING)) {
            call = service.getPendingComments(auth(context), GET_MAX_SIZE, GET_SORT_BY, GET_SORT_TYPE);
        } else {
            call = service.getDisapprovedComments(auth(context), GET_MAX_SIZE, GET_SORT_BY, GET_SORT_TYPE);
        }
        call.enqueue(new Callback<List<CommentAdmin>>() {
            @Override
            public void onResponse(Call<List<CommentAdmin>> call, Response<List<CommentAdmin>> response) {
                if (response.isSuccessful()) {
                    adminCommentsListener.onCommentsLoad(response.body(), visibility);
                } else {
                    logErrorBody(response);
                    adminCommentsListener.onRequestError();
                }
            }

            @Override
            public void onFailure(Call<List<CommentAdmin>> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
                adminCommentsListener.onRequestError();
            }
        });
    }

    public void updateCommentVisibility(Context context, String commentId, Visibility visibility) {
        Call<CommentAdmin> call = service.patchCommentVisibility(auth(context), commentId, visibility);
        call.enqueue(new Callback<CommentAdmin>() {
            @Override
            public void onResponse(Call<CommentAdmin> call, Response<CommentAdmin> response) {
                if (response.isSuccessful()) {
                    adminCommentsListener.onRequestSuccess(response.body());
                } else {
                    logErrorBody(response);
                    adminCommentsListener.onRequestError();
                }
            }

            @Override
            public void onFailure(Call<CommentAdmin> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
                adminCommentsListener.onRequestError();
            }
        });
    }

    public void updateComment(Context context, String commentId, CommentUpdate commentUpdate) {
        Call<CommentAdmin> call = service.patchComment(auth(context), commentId, commentUpdate);
        call.enqueue(new Callback<CommentAdmin>() {
            @Override
            public void onResponse(Call<CommentAdmin> call, Response<CommentAdmin> response) {
                if (response.isSuccessful()) {
                    adminCommentsListener.onRequestSuccess(response.body());
                } else {
                    logErrorBody(response);
                    adminCommentsListener.onRequestError();
                }
            }

            @Override
            public void onFailure(Call<CommentAdmin> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
                adminCommentsListener.onRequestError();
            }
        });
    }

    public void deleteComment(Context context, String commentId) {
        Call<Void> call = service.deleteComment(auth(context), commentId);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    adminCommentsListener.onRequestSuccess();
                } else {
                    logErrorBody(response);
                    adminCommentsListener.onRequestError();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
                adminCommentsListener.onRequestError();
            }
        });
    }

    public void getCommentsIncident(Context context, String commentId) {
        Call<IncidentAdmin> call = service.getCommentsIncident(auth(context), commentId);
        call.enqueue(new Callback<IncidentAdmin>() {
            @Override
            public void onResponse(Call<IncidentAdmin> call, Response<IncidentAdmin> response) {
                if (response.isSuccessful()) {
                    adminCommentsListener.onCommentIncidentLoad(response.body());
                } else {
                    logErrorBody(response);
                    adminCommentsListener.onRequestError();
                }
            }

            @Override
            public void onFailure(Call<IncidentAdmin> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
                adminCommentsListener.onRequestError();
            }
        });
    }

    public void getIncidents(Context context, final Visibility visibility) {
        Call<List<IncidentAdminExcerpt>> call;
        if (visibility.equals(Visibility.PENDING)) {
            call = service.getPendingIncidents(auth(context), GET_MAX_SIZE, GET_SORT_BY, GET_SORT_TYPE);
        } else {
            call = service.getDisapprovedIncidents(auth(context), GET_MAX_SIZE, GET_SORT_BY, GET_SORT_TYPE);
        }
        call.enqueue(new Callback<List<IncidentAdminExcerpt>>() {
            @Override
            public void onResponse(Call<List<IncidentAdminExcerpt>> call, Response<List<IncidentAdminExcerpt>> response) {
                if (response.isSuccessful()) {
                    adminIncidentsListener.onIncidentsLoad(response.body(), visibility);
                } else {
                    logErrorBody(response);
                    adminIncidentsListener.onRequestError();
                }
            }

            @Override
            public void onFailure(Call<List<IncidentAdminExcerpt>> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
                adminIncidentsListener.onRequestError();
            }
        });
    }

    public void getIncidentDetail(Context context, String incidentId) {
        Call<IncidentAdmin> call = service.getIncidentDetail(auth(context), incidentId);
        call.enqueue(new Callback<IncidentAdmin>() {
            @Override
            public void onResponse(Call<IncidentAdmin> call, Response<IncidentAdmin> response) {
                if (response.isSuccessful()) {
                    adminIncidentDataListener.onIncidentDataLoad(response.body());
                } else {
                    logErrorBody(response);
                    adminIncidentDataListener.onRequestError();
                }
            }

            @Override
            public void onFailure(Call<IncidentAdmin> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
                adminIncidentDataListener.onRequestError();
            }
        });
    }

    public void getIncidentsComments(Context context, String incidentId) {
        Call<List<CommentAdmin>> call = service.getIncidentComments(auth(context), incidentId, GET_MAX_SIZE, GET_SORT_BY, GET_SORT_TYPE);
        call.enqueue(new Callback<List<CommentAdmin>>() {
            @Override
            public void onResponse(Call<List<CommentAdmin>> call, Response<List<CommentAdmin>> response) {
                if (response.isSuccessful()) {
                    adminIncidentCommentsListener.onCommentsLoad(response.body());
                } else {
                    logErrorBody(response);
                    adminIncidentCommentsListener.onCommentsLoadError();
                }
            }

            @Override
            public void onFailure(Call<List<CommentAdmin>> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
                adminIncidentCommentsListener.onCommentsLoadError();
            }
        });
    }

    public void getIncidentsAttachments(Context context, String incidentId) {
        Call<List<AttachmentAdmin>> call = service.getIncidentAttachments(auth(context), incidentId, GET_MAX_SIZE, GET_SORT_BY, GET_SORT_TYPE);
        call.enqueue(new Callback<List<AttachmentAdmin>>() {
            @Override
            public void onResponse(Call<List<AttachmentAdmin>> call, Response<List<AttachmentAdmin>> response) {
                if (response.isSuccessful()) {
                    adminIncidentAttachmentsListener.onAttachmentsLoad(response.body());
                } else {
                    logErrorBody(response);
                    adminIncidentAttachmentsListener.onRequestError();
                }
            }

            @Override
            public void onFailure(Call<List<AttachmentAdmin>> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
                adminIncidentAttachmentsListener.onRequestError();
            }
        });
    }

    public void deleteAttachment(Context context, String attachmentId) {
        Call<Void> call = service.deleteAttachment(auth(context), attachmentId);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    adminIncidentAttachmentsListener.onRequestSuccess();
                } else {
                    logErrorBody(response);
                    adminIncidentAttachmentsListener.onRequestError();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
                adminIncidentAttachmentsListener.onRequestError();
            }
        });
    }

    public void patchIncidentVisibility(Context context, String incidentId, Visibility visibility) {
        Call<IncidentAdmin> call = service.patchIncidentVisibility(auth(context), incidentId, visibility);
        call.enqueue(new Callback<IncidentAdmin>() {
            @Override
            public void onResponse(Call<IncidentAdmin> call, Response<IncidentAdmin> response) {
                if (response.isSuccessful()) {
                    adminIncidentDetailListener.onRequestSuccess(response.body());
                } else {
                    logErrorBody(response);
                    adminIncidentDetailListener.onRequestError();
                }
            }

            @Override
            public void onFailure(Call<IncidentAdmin> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
                adminIncidentDetailListener.onRequestError();
            }
        });
    }

    public void deleteIncident(Context context, String incidentId) {
        Call<Void> call = service.deleteIncident(auth(context), incidentId);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    adminIncidentDetailListener.onIncidentDelete();
                } else {
                    logErrorBody(response);
                    adminIncidentDetailListener.onRequestError();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
                adminIncidentDetailListener.onRequestError();
            }
        });
    }

    public void updateIncident(Context context, String incidentId, IncidentUpdate incidentUpdate) {
        Call<IncidentAdmin> call = service.patchIncident(auth(context), incidentId, incidentUpdate);
        call.enqueue(new Callback<IncidentAdmin>() {
            @Override
            public void onResponse(Call<IncidentAdmin> call, Response<IncidentAdmin> response) {
                if (response.isSuccessful()) {
                    adminIncidentUpdateListener.onUpdateSuccess(response.body());
                } else {
                    logErrorBody(response);
                    adminIncidentUpdateListener.onUpdateFail();
                }
            }

            @Override
            public void onFailure(Call<IncidentAdmin> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
                adminIncidentUpdateListener.onUpdateFail();
            }
        });
    }

    private void logErrorBody(Response response) {
        Log.e(TAG, "Error while requesting server. Status code: " + response.code());
        try {
            Log.e(TAG, "Error while requesting server. Response body: " + response.errorBody().string());
        } catch (IOException e) {
            // do nothing
        }
    }

    private String auth(Context context) {
        return AuthUtils.getAuthTokenWithPrefix(context);
    }
}
