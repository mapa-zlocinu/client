/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.model.admin;

import java.io.Serializable;
import java.util.Map;

public class DataSummary implements Serializable {
    private int numberOfIncidents;
    private int numberOfComments;
    private int numberOfAttachments;
    private int numberOfUsers;

    private Map<String, Long> incidentsByVisibility;
    private Map<String, Long> incidentsByType;
    private Map<String, Long> commentsByVisibility;

    public int getNumberOfIncidents() {
        return numberOfIncidents;
    }

    public void setNumberOfIncidents(int numberOfIncidents) {
        this.numberOfIncidents = numberOfIncidents;
    }

    public int getNumberOfComments() {
        return numberOfComments;
    }

    public void setNumberOfComments(int numberOfComments) {
        this.numberOfComments = numberOfComments;
    }

    public int getNumberOfAttachments() {
        return numberOfAttachments;
    }

    public void setNumberOfAttachments(int numberOfAttachments) {
        this.numberOfAttachments = numberOfAttachments;
    }

    public int getNumberOfUsers() {
        return numberOfUsers;
    }

    public void setNumberOfUsers(int numberOfUsers) {
        this.numberOfUsers = numberOfUsers;
    }

    public Map<String, Long> getIncidentsByVisibility() {
        return incidentsByVisibility;
    }

    public void setIncidentsByVisibility(Map<String, Long> incidentsByVisibility) {
        this.incidentsByVisibility = incidentsByVisibility;
    }

    public Map<String, Long> getIncidentsByType() {
        return incidentsByType;
    }

    public void setIncidentsByType(Map<String, Long> incidentsByType) {
        this.incidentsByType = incidentsByType;
    }

    public Map<String, Long> getCommentsByVisibility() {
        return commentsByVisibility;
    }

    public void setCommentsByVisibility(Map<String, Long> commentsByVisibility) {
        this.commentsByVisibility = commentsByVisibility;
    }
}
