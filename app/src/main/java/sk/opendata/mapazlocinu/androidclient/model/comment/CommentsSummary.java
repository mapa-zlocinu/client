/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.model.comment;

public class CommentsSummary {
    private int totalNum;
    private int confirmationSum;
    private int refutationSum;
    private int neutralSum;

    public CommentsSummary() {
        this.totalNum = 0;
        this.confirmationSum = 0;
        this.refutationSum = 0;
        this.neutralSum = 0;
    }

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    public int getConfirmationSum() {
        return confirmationSum;
    }

    public void setConfirmationSum(int confirmationSum) {
        this.confirmationSum = confirmationSum;
    }

    public int getRefutationSum() {
        return refutationSum;
    }

    public void setRefutationSum(int refutationSum) {
        this.refutationSum = refutationSum;
    }

    public int getNeutralSum() {
        return neutralSum;
    }

    public void setNeutralSum(int neutralSum) {
        this.neutralSum = neutralSum;
    }

    public void addConfirmation() {
        this.confirmationSum++;
    }

    public void addRefutation() {
        this.refutationSum++;
    }

    public void addNeutral() {
        this.neutralSum++;
    }
}
