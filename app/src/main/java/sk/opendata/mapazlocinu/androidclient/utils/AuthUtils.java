/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.utils;

import android.content.Context;

import java.util.Calendar;

import sk.opendata.mapazlocinu.androidclient.model.security.LoginRequest;
import sk.opendata.mapazlocinu.androidclient.model.security.LoginResponse;

public class AuthUtils {
    private static final String PREFS_AUTH_GROUP_KEY = "sk.mapazlocinu.auth0";
    private static final String PREFS_NAME_KEY = "sk.mapazlocinu.auth0.name";
    private static final String PREFS_DATE_KEY = "sk.mapazlocinu.auth0.date";
    private static final String PREFS_TOKEN_KEY = "sk.mapazlocinu.auth0.token";

    public static void storeAuthDetails(Context context, LoginResponse loginResponse, LoginRequest loginRequest) {
        context.getSharedPreferences(PREFS_AUTH_GROUP_KEY, Context.MODE_PRIVATE).edit()
                .putString(PREFS_TOKEN_KEY, loginResponse.getToken())
                .putString(PREFS_NAME_KEY, loginRequest.getUsername())
                .putLong(PREFS_DATE_KEY, Calendar.getInstance().getTimeInMillis())
                .apply();
    }

    public static void deleteAuthDetails(Context context) {
        context.getSharedPreferences(PREFS_AUTH_GROUP_KEY, Context.MODE_PRIVATE).edit()
                .remove(PREFS_NAME_KEY)
                .remove(PREFS_DATE_KEY)
                .remove(PREFS_TOKEN_KEY)
                .apply();
    }

    public static String getAuthName(Context context) {
        return context.getSharedPreferences(PREFS_AUTH_GROUP_KEY, Context.MODE_PRIVATE).getString(PREFS_NAME_KEY, "");
    }

    public static long getAuthDate(Context context) {
        return context.getSharedPreferences(PREFS_AUTH_GROUP_KEY, Context.MODE_PRIVATE).getLong(PREFS_DATE_KEY, -1L);
    }

    public static String getAuthToken(Context context) {
        return context.getSharedPreferences(PREFS_AUTH_GROUP_KEY, Context.MODE_PRIVATE).getString(PREFS_TOKEN_KEY, "");
    }

    public static String getAuthTokenWithPrefix(Context context) {
        return "Bearer " + context.getSharedPreferences(PREFS_AUTH_GROUP_KEY, Context.MODE_PRIVATE).getString(PREFS_TOKEN_KEY, "");
    }
}
