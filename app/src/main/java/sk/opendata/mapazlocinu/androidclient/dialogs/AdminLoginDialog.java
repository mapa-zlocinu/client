/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import sk.opendata.mapazlocinu.androidclient.R;

public class AdminLoginDialog extends DialogFragment {
    private AdminDialogInterface listener;

    @SuppressWarnings("InflateParams")
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View view = inflater.inflate(R.layout.admin_login_dialog, null);

        builder.setView(view)
                .setTitle(R.string.admin_login_title)
                .setIcon(R.mipmap.ic_launcher)
                .setPositiveButton(R.string.login, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String name = ((TextView) view.findViewById(R.id.admin_name)).getText().toString();
                        String pass = ((TextView) view.findViewById(R.id.admin_pass)).getText().toString();

                        listener.onAdminLoginSubmit(name, pass);
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton(R.string.about_close, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (AdminDialogInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AdminDialogInterface!");
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (AdminDialogInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement AdminDialogInterface!");
        }
    }

    public interface AdminDialogInterface {
        void onAdminLoginSubmit(String name, String pass);
    }
}
