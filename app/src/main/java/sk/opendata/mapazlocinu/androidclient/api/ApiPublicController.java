/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.api;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import sk.opendata.mapazlocinu.androidclient.api.config.ApiClient;
import sk.opendata.mapazlocinu.androidclient.api.interfaces.PublicIncidentsApiInterface;
import sk.opendata.mapazlocinu.androidclient.api.listeners.CommentCreateListener;
import sk.opendata.mapazlocinu.androidclient.api.listeners.IncidentAndAttachmentCreateListener;
import sk.opendata.mapazlocinu.androidclient.api.listeners.PublicAttachmentsLoadListener;
import sk.opendata.mapazlocinu.androidclient.api.listeners.PublicCommentsLoadListener;
import sk.opendata.mapazlocinu.androidclient.api.listeners.PublicIncidentDataLoadListener;
import sk.opendata.mapazlocinu.androidclient.api.listeners.PublicIncidentsLoadListener;
import sk.opendata.mapazlocinu.androidclient.model.CreateItemResponse;
import sk.opendata.mapazlocinu.androidclient.model.attachment.AttachmentPublic;
import sk.opendata.mapazlocinu.androidclient.model.comment.CommentCreate;
import sk.opendata.mapazlocinu.androidclient.model.comment.CommentPublic;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentCreate;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentPublic;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentPublicExcerpt;
import sk.opendata.mapazlocinu.androidclient.utils.files.FileUtils;

public class ApiPublicController {
    private static final String TAG = "ApiPublicController";
    private static final int GET_MAX_SIZE = 3500;
    private static final String GET_SORT_BY = "date";
    private static final String GET_SORT_TYPE = "desc";

    private static ApiPublicController apiController;
    private final PublicIncidentsApiInterface service;

    private PublicIncidentsLoadListener publicIncidentsLoadListener;
    private PublicAttachmentsLoadListener publicAttachmentsLoadListener;
    private PublicCommentsLoadListener publicCommentsLoadListener;
    private PublicIncidentDataLoadListener publicIncidentDataLoadListener;
    private CommentCreateListener commentCreateListener;
    private IncidentAndAttachmentCreateListener incidentAndAttachmentCreateListener;

    private ApiPublicController() {
        Retrofit retrofit = ApiClient.getInstance();
        this.service = retrofit.create(PublicIncidentsApiInterface.class);
    }

    public static ApiPublicController getInstance() {
        if (apiController == null) {
            apiController = new ApiPublicController();
        }

        return apiController;
    }

    public void registerListener(PublicIncidentsLoadListener listener) {
        this.publicIncidentsLoadListener = listener;
    }

    public void registerListener(PublicAttachmentsLoadListener listener) {
        this.publicAttachmentsLoadListener = listener;
    }

    public void registerListener(PublicCommentsLoadListener listener) {
        this.publicCommentsLoadListener = listener;
    }

    public void registerListener(PublicIncidentDataLoadListener listener) {
        this.publicIncidentDataLoadListener = listener;
    }

    public void registerListener(CommentCreateListener listener) {
        this.commentCreateListener = listener;
    }

    public void registerListener(IncidentAndAttachmentCreateListener listener) {
        this.incidentAndAttachmentCreateListener = listener;
    }

    public void loadPublicIncidents() {
        Call<List<IncidentPublicExcerpt>> call = service.getPublicIncidents(GET_MAX_SIZE, GET_SORT_BY, GET_SORT_TYPE);
        call.enqueue(new Callback<List<IncidentPublicExcerpt>>() {
            @Override
            public void onResponse(Call<List<IncidentPublicExcerpt>> call, Response<List<IncidentPublicExcerpt>> response) {
                if (response.isSuccessful()) {
                    publicIncidentsLoadListener.incidentsLoaded(response.body());
                } else {
                    publicIncidentsLoadListener.onLoadError();
                    logErrorBody(response);
                }
            }

            @Override
            public void onFailure(Call<List<IncidentPublicExcerpt>> call, Throwable t) {
                publicIncidentsLoadListener.onLoadError();
                Log.e(TAG, "Error while requesting server.", t);
            }
        });
    }

    public void loadPublicIncidentData(String incidentId) {
        Call<IncidentPublic> call = service.getPublicIncidentDetail(incidentId);
        call.enqueue(new Callback<IncidentPublic>() {
            @Override
            public void onResponse(Call<IncidentPublic> call, Response<IncidentPublic> response) {
                if (response.isSuccessful()) {
                    publicIncidentDataLoadListener.onPublicIncidentDataLoad(response.body());
                } else {
                    logErrorBody(response);
                }
            }

            @Override
            public void onFailure(Call<IncidentPublic> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
            }
        });
    }

    public void loadPublicIncidentsComments(String incidentId) {
        Call<List<CommentPublic>> call = service.getPublicIncidentComments(incidentId, GET_MAX_SIZE, GET_SORT_BY, GET_SORT_TYPE);
        call.enqueue(new Callback<List<CommentPublic>>() {
            @Override
            public void onResponse(Call<List<CommentPublic>> call, Response<List<CommentPublic>> response) {
                if (response.isSuccessful()) {
                    publicCommentsLoadListener.onIncidentCommentsLoad(response.body());
                } else {
                    logErrorBody(response);
                }
            }

            @Override
            public void onFailure(Call<List<CommentPublic>> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
            }
        });
    }

    public void loadPublicIncidentsAttachments(String incidentId) {
        Call<List<AttachmentPublic>> call = service.getPublicIncidentAttachments(incidentId, GET_MAX_SIZE, GET_SORT_BY, GET_SORT_TYPE);
        call.enqueue(new Callback<List<AttachmentPublic>>() {
            @Override
            public void onResponse(Call<List<AttachmentPublic>> call, Response<List<AttachmentPublic>> response) {
                if (response.isSuccessful()) {
                    publicAttachmentsLoadListener.onIncidentAttachmentsLoad(response.body());
                } else {
                    logErrorBody(response);
                }
            }

            @Override
            public void onFailure(Call<List<AttachmentPublic>> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
            }
        });
    }

    public void createComment(String incidentId, CommentCreate commentCreate) {
        Call<CreateItemResponse> call = service.postNewComment(incidentId, commentCreate);
        call.enqueue(new Callback<CreateItemResponse>() {
            @Override
            public void onResponse(Call<CreateItemResponse> call, Response<CreateItemResponse> response) {
                if (response.isSuccessful()) {
                    commentCreateListener.onCommentCreated(response.body());
                } else {
                    logErrorBody(response);
                    commentCreateListener.onCommentCreateFailure();
                }
            }

            @Override
            public void onFailure(Call<CreateItemResponse> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
                commentCreateListener.onCommentCreateFailure();
            }
        });
    }

    public void createIncident(IncidentCreate incidentCreate) {
        Call<CreateItemResponse> call = service.postNewIncident(incidentCreate);
        call.enqueue(new Callback<CreateItemResponse>() {
            @Override
            public void onResponse(Call<CreateItemResponse> call, Response<CreateItemResponse> response) {
                if (response.isSuccessful()) {
                    incidentAndAttachmentCreateListener.onIncidentCreateSuccess(response.body());
                } else {
                    logErrorBody(response);
                    incidentAndAttachmentCreateListener.onIncidentCreateFailure();
                }
            }

            @Override
            public void onFailure(Call<CreateItemResponse> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
                incidentAndAttachmentCreateListener.onIncidentCreateFailure();
            }
        });
    }

    public void createAttachment(final String incidentId, Uri fileUri, Context context, ContentResolver contentResolver) {
        MultipartBody.Part body;
        try {
            File file = FileUtils.getFile(context, fileUri);
            RequestBody requestBodyFile = RequestBody.create(MediaType.parse(contentResolver.getType(fileUri)), file);
            body = MultipartBody.Part.createFormData("file", file.getName(), requestBodyFile);
        } catch (Exception e) {
            Log.e(TAG, "Error while getting file to upload.", e);
            incidentAndAttachmentCreateListener.onAttachmentCreateFailure(incidentId);
            return;
        }

        Call<CreateItemResponse> call = service.postNewAttachment(incidentId, body);
        call.enqueue(new Callback<CreateItemResponse>() {
            @Override
            public void onResponse(Call<CreateItemResponse> call, Response<CreateItemResponse> response) {
                if (response.isSuccessful()) {
                    incidentAndAttachmentCreateListener.onAttachmentCreateSuccess(incidentId);
                } else {
                    logErrorBody(response);
                    incidentAndAttachmentCreateListener.onAttachmentCreateFailure(incidentId);
                }
            }

            @Override
            public void onFailure(Call<CreateItemResponse> call, Throwable t) {
                Log.e(TAG, "Error while requesting server.", t);
                incidentAndAttachmentCreateListener.onAttachmentCreateFailure(incidentId);
            }
        });
    }

    private void logErrorBody(Response response) {
        Log.e(TAG, "Error while requesting server. Status code: " + response.code());
        try {
            Log.e(TAG, "Error while requesting server. Response body: " + response.errorBody().string());
        } catch (IOException e) {
            // do nothing
        }
    }
}
