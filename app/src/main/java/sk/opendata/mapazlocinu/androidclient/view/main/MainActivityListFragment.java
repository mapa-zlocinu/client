/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.view.main;


import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.adapters.IncidentsListAdapter;
import sk.opendata.mapazlocinu.androidclient.api.ApiPublicController;
import sk.opendata.mapazlocinu.androidclient.api.listeners.PublicIncidentsLoadListener;
import sk.opendata.mapazlocinu.androidclient.data.MainActivityDataHolder;
import sk.opendata.mapazlocinu.androidclient.dividers.ItemDecorationDivider;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentPublicExcerpt;

public class MainActivityListFragment extends Fragment implements PublicIncidentsLoadListener {
    private SwipeRefreshLayout swipeRefreshLayout;
    private IncidentsListAdapter adapter;

    public MainActivityListFragment() {
    }

    public static Fragment newInstance() {
        return new MainActivityListFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_activity_list, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.incidents_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new IncidentsListAdapter(MainActivityDataHolder.getInstance().getPublicIncidents(), getActivity());

        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new ItemDecorationDivider(getActivity()));

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.incidents_recycler_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ApiPublicController.getInstance().registerListener(MainActivityListFragment.this);
                ApiPublicController.getInstance().loadPublicIncidents();
            }
        });

        return view;
    }

    public void refreshData() {
        List<IncidentPublicExcerpt> incidents = MainActivityDataHolder.getInstance().getPublicIncidents();
        if (incidents == null || incidents.size() == 0) {
            Toast.makeText(getActivity(), R.string.no_data, Toast.LENGTH_SHORT).show();
        }

        adapter.addNewDataSet(incidents);
    }

    @Override
    public void incidentsLoaded(List<IncidentPublicExcerpt> incidents) {
        if (incidents != null) {
            if (swipeRefreshLayout.isRefreshing()) {
                swipeRefreshLayout.setRefreshing(false);
            }

            MainActivityDataHolder.getInstance().setPublicIncidents(incidents);
            refreshData();
        } else {
            Toast.makeText(getActivity(), R.string.error_loading_data, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onLoadError() {
        Toast.makeText(getActivity(), R.string.error_loading_data, Toast.LENGTH_LONG).show();
    }
}
