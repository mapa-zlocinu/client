/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.joaquimley.faboptions.FabOptions;

import sk.opendata.mapazlocinu.androidclient.adapters.AdminIncidentPagerAdapter;
import sk.opendata.mapazlocinu.androidclient.api.ApiAdminController;
import sk.opendata.mapazlocinu.androidclient.api.listeners.admin.AdminIncidentDetailListener;
import sk.opendata.mapazlocinu.androidclient.data.AdminActivityDataHolder;
import sk.opendata.mapazlocinu.androidclient.dialogs.AdminIncidentDeleteDialog;
import sk.opendata.mapazlocinu.androidclient.model.enums.Visibility;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentAdmin;

public class AdminIncidentDetailActivity extends AppCompatActivity implements AdminIncidentDeleteDialog.AdminDeleteDialogListener, AdminIncidentDetailListener {
    public static final String INCIDENT_ID_KEY = "AdminIncidentDetailActivity.key";
    public static final String INCIDENT_OBJECT_KEY = "AdminIncidentDetailActivity.object";

    private String incidentId;
    private IncidentAdmin incident;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_incident_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        this.incidentId = getIntent().getStringExtra(INCIDENT_ID_KEY);
        this.incident = (IncidentAdmin) getIntent().getSerializableExtra(INCIDENT_OBJECT_KEY);

        if (this.incidentId == null && this.incident != null) {
            this.incidentId = this.incident.getId();
        }

        if (this.incidentId != null) {
            AdminActivityDataHolder.getInstance().setAdminIncidentDetailId(this.incidentId);
        } else {
            this.incidentId = AdminActivityDataHolder.getInstance().getAdminIncidentDetailId();
        }

        ApiAdminController.getInstance().registerListener(this);

        AdminIncidentPagerAdapter mAdminIncidentPagerAdapter = new AdminIncidentPagerAdapter(getFragmentManager(), getApplicationContext());
        ViewPager mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mAdminIncidentPagerAdapter);
        mViewPager.setOffscreenPageLimit(3);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        FabOptions fabOptions = (FabOptions) findViewById(R.id.fab_options);
        fabOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.inc_opt_edit:
                        Intent intent = new Intent(getApplicationContext(), EditIncidentActivity.class);
                        intent.putExtra(EditIncidentActivity.INCIDENT_DATA_KEY, incident);
                        startActivity(intent);
                        break;
                    case R.id.inc_opt_delete:
                        new AdminIncidentDeleteDialog().show(getFragmentManager(), "AdminIncidentDeleteDialog");
                        break;
                    case R.id.inc_opt_approve:
                        ApiAdminController.getInstance().patchIncidentVisibility(getApplicationContext(), incidentId, Visibility.APPROVED);
                        break;
                    case R.id.inc_opt_disapprove:
                        ApiAdminController.getInstance().patchIncidentVisibility(getApplicationContext(), incidentId, Visibility.DISAPPROVED);
                        break;
                    default:
                        // do nothing
                        break;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public String getIncidentId() {
        return incidentId;
    }

    public IncidentAdmin getIncident() {
        return incident;
    }

    public void setIncident(IncidentAdmin incident) {
        this.incident = incident;
    }

    @Override
    public void onDeleteRequest() {
        ApiAdminController.getInstance().deleteIncident(getApplicationContext(), incidentId);
    }

    @Override
    public void onRequestSuccess(IncidentAdmin incident) {
        Toast.makeText(getApplicationContext(), R.string.operation_done, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestError() {
        Toast.makeText(getApplicationContext(), R.string.admin_data_fail, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onIncidentDelete() {
        Toast.makeText(getApplicationContext(), R.string.operation_done, Toast.LENGTH_LONG).show();
        finish();
    }
}
