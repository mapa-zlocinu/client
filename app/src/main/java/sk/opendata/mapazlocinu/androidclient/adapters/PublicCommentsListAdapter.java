/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.model.comment.CommentPublic;
import sk.opendata.mapazlocinu.androidclient.model.comment.CommentsSummary;
import sk.opendata.mapazlocinu.androidclient.model.enums.CommentType;
import sk.opendata.mapazlocinu.androidclient.utils.IncidentsUtils;

public class PublicCommentsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int SUMMARY_VIEW_TYPE = 1;
    private static final int COMMENT_VIEW_TYPE = 2;

    private final Context context;
    private List<CommentPublic> comments;
    private CommentsSummary commentsSummary;

    public PublicCommentsListAdapter(List<CommentPublic> comments, Context context) {
        this.comments = comments != null ? comments : new ArrayList<CommentPublic>();
        this.context = context;

        createCommentsSummary();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == SUMMARY_VIEW_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_summary_list_item, parent, false);
            return new CommentsSumViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_list_item, parent, false);
            return new CommentViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CommentsSumViewHolder) {
            CommentsSumViewHolder viewHolder = (CommentsSumViewHolder) holder;

            viewHolder.totalSum.setText(String.valueOf(commentsSummary.getTotalNum()));
            viewHolder.confirmationSum.setText(String.valueOf(commentsSummary.getConfirmationSum()));
            viewHolder.refutationSum.setText(String.valueOf(commentsSummary.getRefutationSum()));
            viewHolder.neutralSum.setText(String.valueOf(commentsSummary.getNeutralSum()));
        } else {
            CommentViewHolder viewHolder = (CommentViewHolder) holder;
            CommentPublic item = comments.get(position - 1);

            if (item.isAuthorVisible()) {
                viewHolder.commentAuthor.setText(item.getAuthor().getName());
                viewHolder.commentAuthorEmail.setText("/" + item.getAuthor().getEmail() + "/");
            } else {
                viewHolder.commentAuthor.setText(context.getString(R.string.comment_anonymous));
                viewHolder.commentAuthorEmail.setText("");
            }
            viewHolder.commentDate.setText(IncidentsUtils.toFormattedDate(item.getCreatedDate()));
            viewHolder.commentType.setText(context.getString(IncidentsUtils.commentTypeToStringResource(item.getType())));
            viewHolder.commentText.setText(item.getText());
        }
    }

    @Override
    public int getItemCount() {
        return this.comments.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return SUMMARY_VIEW_TYPE;
        }

        return COMMENT_VIEW_TYPE;
    }

    public void addNewDataSet(List<CommentPublic> comments) {
        this.comments = comments != null ? comments : new ArrayList<CommentPublic>();
        createCommentsSummary();
        notifyDataSetChanged();
    }

    private void createCommentsSummary() {
        commentsSummary = new CommentsSummary();

        for (CommentPublic commentPublic : comments) {
            if (commentPublic.getType().equals(CommentType.CONFIRMATION)) {
                commentsSummary.addConfirmation();
            } else if (commentPublic.getType().equals(CommentType.REFUTATION)) {
                commentsSummary.addRefutation();
            } else {
                commentsSummary.addNeutral();
            }
        }

        commentsSummary.setTotalNum(comments.size());
    }

    private class CommentViewHolder extends RecyclerView.ViewHolder {
        final View view;
        final TextView commentAuthor;
        final TextView commentAuthorEmail;
        final TextView commentDate;
        final TextView commentType;
        final TextView commentText;

        CommentViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;
            this.commentAuthor = (TextView) view.findViewById(R.id.comment_author);
            this.commentAuthorEmail = (TextView) view.findViewById(R.id.comment_author_email);
            this.commentDate = (TextView) view.findViewById(R.id.comment_date);
            this.commentType = (TextView) view.findViewById(R.id.comment_type);
            this.commentText = (TextView) view.findViewById(R.id.comment_text);
        }
    }

    private class CommentsSumViewHolder extends RecyclerView.ViewHolder {
        final View view;
        final TextView totalSum;
        final TextView confirmationSum;
        final TextView refutationSum;
        final TextView neutralSum;

        CommentsSumViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;
            this.totalSum = (TextView) view.findViewById(R.id.comments_sum_total_count);
            this.confirmationSum = (TextView) view.findViewById(R.id.comments_sum_confirmation);
            this.refutationSum = (TextView) view.findViewById(R.id.comments_sum_refutation);
            this.neutralSum = (TextView) view.findViewById(R.id.comments_sum_neutral);
        }
    }
}
