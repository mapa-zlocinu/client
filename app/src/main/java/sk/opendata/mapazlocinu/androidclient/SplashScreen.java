/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.util.List;

import sk.opendata.mapazlocinu.androidclient.api.ApiPublicController;
import sk.opendata.mapazlocinu.androidclient.api.listeners.PublicIncidentsLoadListener;
import sk.opendata.mapazlocinu.androidclient.data.MainActivityDataHolder;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentPublicExcerpt;
import sk.opendata.mapazlocinu.androidclient.utils.Utils;

public class SplashScreen extends AppCompatActivity implements PublicIncidentsLoadListener {
    private static final int SPLASH_TIME_OUT = 3000;
    private boolean firstPartFinished = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

//        Utils.hideNavigation(getWindow());
//        Utils.hideStatusBar(getWindow());

        Utils.isDeviceConnected(getApplicationContext(), true);

        ApiPublicController.getInstance().registerListener(this);
        ApiPublicController.getInstance().loadPublicIncidents();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startMainActivity();
            }
        }, SPLASH_TIME_OUT);
    }

    private void startMainActivity() {
        if (!firstPartFinished) {
            firstPartFinished = true;
            return;
        }

        Intent i = new Intent(SplashScreen.this, MainActivity.class);
        startActivity(i);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        finish();
    }

    @Override
    public void incidentsLoaded(List<IncidentPublicExcerpt> incidents) {
        if (incidents != null) {
            MainActivityDataHolder.getInstance().setPublicIncidents(incidents);
            startMainActivity();
        } else {
            Toast.makeText(getApplicationContext(), R.string.error_loading_data, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onLoadError() {
        Toast.makeText(getApplicationContext(), R.string.error_loading_data, Toast.LENGTH_LONG).show();
    }
}
