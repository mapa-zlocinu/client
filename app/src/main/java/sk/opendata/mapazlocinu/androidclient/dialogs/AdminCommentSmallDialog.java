/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.model.comment.CommentAdmin;
import sk.opendata.mapazlocinu.androidclient.model.enums.Visibility;

public class AdminCommentSmallDialog extends DialogFragment {
    private static final String COMMENT_ID_KEY = "AdminCommentSmallDialog.key";
    private static final String COMMENT_ID_VISIBILITY_KEY = "AdminCommentSmallDialog.vis";
    private static final String INCIDENT_BUTTON_KEY = "AdminCommentSmallDialog.button";

    public static AdminCommentSmallDialog newInstance(CommentAdmin comment, boolean showIncidentButton) {
        AdminCommentSmallDialog dialog = new AdminCommentSmallDialog();

        Bundle bundle = new Bundle();
        bundle.putString(COMMENT_ID_KEY, comment.getId());
        bundle.putString(COMMENT_ID_VISIBILITY_KEY, comment.getVisibility().name());
        bundle.putBoolean(INCIDENT_BUTTON_KEY, showIncidentButton);
        dialog.setArguments(bundle);

        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        boolean showIncidentButton = getArguments().getBoolean(INCIDENT_BUTTON_KEY);
        final String commentId = getArguments().getString(COMMENT_ID_KEY);
        final Visibility visibility = Visibility.valueOf(getArguments().getString(COMMENT_ID_VISIBILITY_KEY));

        CharSequence[] visibilityItems = new CharSequence[2];
        visibilityItems[0] = getString(R.string.visibility_approved);
        visibilityItems[1] = getString(R.string.visibility_disapproved);
        int checkedItem;
        if (visibility.equals(Visibility.DISAPPROVED)) {
            checkedItem = 1;
        } else {
            checkedItem = 0;
        }

        final int[] chosenIndex = {checkedItem};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.comment_update)
                .setSingleChoiceItems(visibilityItems, checkedItem, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        chosenIndex[0] = i;
                    }
                })
                .setNeutralButton(R.string.about_close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(R.string.dialog_update2, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (chosenIndex[0] == 0) {
                            ((AdminCommentSmallDialogListener) getTargetFragment()).onCommentVisibilityUpdateRequest(commentId, Visibility.APPROVED);
                        } else {
                            ((AdminCommentSmallDialogListener) getTargetFragment()).onCommentVisibilityUpdateRequest(commentId, Visibility.DISAPPROVED);
                        }
                        dialogInterface.dismiss();
                    }
                });

        if (showIncidentButton) {
            builder.setNegativeButton(R.string.incident, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    ((AdminCommentSmallDialogListener) getTargetFragment()).onCommentIncidentShowRequest(commentId);
                    dialogInterface.dismiss();
                }
            });
        }

        return builder.create();
    }

    public interface AdminCommentSmallDialogListener {
        void onCommentVisibilityUpdateRequest(String commentId, Visibility visibility);

        void onCommentIncidentShowRequest(String commentId);
    }
}
