/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.api.interfaces;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import sk.opendata.mapazlocinu.androidclient.model.CreateItemResponse;
import sk.opendata.mapazlocinu.androidclient.model.attachment.AttachmentPublic;
import sk.opendata.mapazlocinu.androidclient.model.comment.CommentCreate;
import sk.opendata.mapazlocinu.androidclient.model.comment.CommentPublic;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentCreate;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentPublic;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentPublicExcerpt;

public interface PublicIncidentsApiInterface {
    @GET("incidents")
    Call<List<IncidentPublicExcerpt>> getPublicIncidents(@Query("maxSize") int maxSize, @Query("sortBy") String sortBy, @Query("sortType") String sortType);

    @GET("incidents/{incidentId}")
    Call<IncidentPublic> getPublicIncidentDetail(@Path("incidentId") String incidentId);

    @GET("incidents/{incidentId}/comments")
    Call<List<CommentPublic>> getPublicIncidentComments(@Path("incidentId") String incidentId, @Query("maxSize") int maxSize, @Query("sortBy") String sortBy,
                                                        @Query("sortType") String sortType);

    @GET("incidents/{incidentId}/attachments")
    Call<List<AttachmentPublic>> getPublicIncidentAttachments(@Path("incidentId") String incidentId, @Query("maxSize") int maxSize, @Query("sortBy") String sortBy,
                                                              @Query("sortType") String sortType);

    @POST("incidents/{incidentId}/comments")
    Call<CreateItemResponse> postNewComment(@Path("incidentId") String incidentId, @Body CommentCreate commentCreate);

    @POST("incidents")
    Call<CreateItemResponse> postNewIncident(@Body IncidentCreate incidentCreate);

    @Multipart
    @POST("incidents/{incidentId}/attachments")
    Call<CreateItemResponse> postNewAttachment(@Path("incidentId") String incidentId, @Part MultipartBody.Part file);
}
