/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;

import sk.opendata.mapazlocinu.androidclient.api.ApiAdminController;
import sk.opendata.mapazlocinu.androidclient.api.listeners.admin.AdminIncidentUpdateListener;
import sk.opendata.mapazlocinu.androidclient.data.AdminActivityDataHolder;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentAdmin;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentUpdate;
import sk.opendata.mapazlocinu.androidclient.utils.IncidentsUtils;
import sk.opendata.mapazlocinu.androidclient.utils.Utils;

public class EditIncidentActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener,
        AdminIncidentUpdateListener {

    public static final String INCIDENT_DATA_KEY = "EditIncidentActivity.data";

    private IncidentAdmin incident;
    private EditText titleEditText;
    private EditText descriptionEditText;
    private Spinner incidentTypeSpinner;
    private Button incidentDateButton;
    private EditText locationEditText;
    private CheckBox authorAnonymousCheckBox;

    private boolean wasTimeSelected;
    private Calendar selectedDateTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_incident);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        this.incident = (IncidentAdmin) getIntent().getSerializableExtra(INCIDENT_DATA_KEY);
        if (this.incident == null) {
            Toast.makeText(getApplicationContext(), R.string.admin_data_fail, Toast.LENGTH_LONG).show();
            return;
        }

        ApiAdminController.getInstance().registerListener(this);
        initView();
    }

    private void initView() {
        titleEditText = (EditText) findViewById(R.id.inc_title);
        descriptionEditText = (EditText) findViewById(R.id.inc_description);
        incidentTypeSpinner = (Spinner) findViewById(R.id.inc_type_spinner);
        incidentDateButton = (Button) findViewById(R.id.inc_date_time);
        locationEditText = (EditText) findViewById(R.id.inc_location);
        authorAnonymousCheckBox = (CheckBox) findViewById(R.id.inc_author_visible);
        Button updateButton = (Button) findViewById(R.id.inc_update);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.incident_type, R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        incidentTypeSpinner.setAdapter(adapter);

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateIncident();
            }
        });

        incidentDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog();
            }
        });

        titleEditText.setText(incident.getTitle());
        descriptionEditText.setText(incident.getDescription());
        incidentTypeSpinner.setSelection(IncidentsUtils.incidentTypeToSpinnerIndex(incident.getType()));
        incidentDateButton.setText(IncidentsUtils.toFormattedDate(incident.getIncidentDate()));
        locationEditText.setText(incident.getLocationAddress());
        authorAnonymousCheckBox.setChecked(!incident.isAuthorVisible());

        wasTimeSelected = true;
        selectedDateTime = Calendar.getInstance();
        selectedDateTime.setTimeInMillis(incident.getIncidentDate());
    }

    private void showDateDialog() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dateDialog = DatePickerDialog.newInstance(this, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
        dateDialog.setMaxDate(now);
        dateDialog.dismissOnPause(true);
        dateDialog.show(getFragmentManager(), "DatePickerDialog");
    }

    private void showTimeDialog() {
        Calendar now = Calendar.getInstance();
        TimePickerDialog dateDialog = TimePickerDialog.newInstance(this, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), true);
        dateDialog.dismissOnPause(true);
        dateDialog.show(getFragmentManager(), "TimePickerDialog");
    }

    private void updateIncident() {
        boolean isError = false;

        if (!Utils.validateMiddleEditText(titleEditText, getApplicationContext()) || !Utils.validateLongEditText(descriptionEditText, getApplicationContext()) ||
                !Utils.validateMiddleEditText(locationEditText, getApplicationContext())) {
            isError = true;
        }

        if (!wasTimeSelected || selectedDateTime == null || selectedDateTime.after(Calendar.getInstance())) {
            isError = true;
            Toast.makeText(getApplicationContext(), R.string.new_inc_err_date, Toast.LENGTH_SHORT).show();
        }

        if (!isError) {
            IncidentUpdate update = new IncidentUpdate();
            update.setTitle(titleEditText.getText().toString());
            update.setDescription(descriptionEditText.getText().toString());
            update.setIncidentDate(selectedDateTime.getTimeInMillis());
            update.setCreatedDate(incident.getCreatedDate());
            update.setAuthorVisible(!authorAnonymousCheckBox.isChecked());
            update.setLat(incident.getLat());
            update.setLon(incident.getLon());
            update.setLocationAddress(locationEditText.getText().toString());
            update.setVisibility(incident.getVisibility());
            update.setType(IncidentsUtils.spinnerIndexToIncidentType(incidentTypeSpinner.getSelectedItemPosition()));

            ApiAdminController.getInstance().updateIncident(getApplicationContext(), incident.getId(), update);
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        wasTimeSelected = false;
        selectedDateTime = Calendar.getInstance();
        selectedDateTime.set(year, monthOfYear, dayOfMonth);
        showTimeDialog();
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        wasTimeSelected = true;
        selectedDateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        selectedDateTime.set(Calendar.MINUTE, minute);
        selectedDateTime.set(Calendar.SECOND, second);

        incidentDateButton.setText(IncidentsUtils.toFormattedDate(selectedDateTime.getTimeInMillis()));
    }

    @Override
    public void onUpdateSuccess(IncidentAdmin incident) {
        Toast.makeText(getApplicationContext(), R.string.operation_done, Toast.LENGTH_LONG).show();
        AdminActivityDataHolder.getInstance().setUpdatedIncident(incident);
        finish();
    }

    @Override
    public void onUpdateFail() {
        Toast.makeText(getApplicationContext(), R.string.admin_data_fail, Toast.LENGTH_LONG).show();
    }
}
