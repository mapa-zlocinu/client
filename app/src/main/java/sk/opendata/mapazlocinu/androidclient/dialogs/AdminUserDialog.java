/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.model.user.UserAdmin;

public class AdminUserDialog extends DialogFragment {
    private static final String USER_NAME_KEY = "AdminUserDialog.name";
    private static final String USER_EMAIL_KEY = "AdminUserDialog.email";
    private static final String USER_RATING_KEY = "AdminUserDialog.rating";

    public static AdminUserDialog newInstance(UserAdmin userAdmin) {
        AdminUserDialog adminUserDialog = new AdminUserDialog();

        Bundle args = new Bundle();
        args.putString(USER_NAME_KEY, userAdmin.getName());
        args.putString(USER_EMAIL_KEY, userAdmin.getEmail());
        args.putInt(USER_RATING_KEY, userAdmin.getRating());
        adminUserDialog.setArguments(args);

        return adminUserDialog;
    }

    @SuppressWarnings("InflateParams")
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final String username = getArguments().getString(USER_NAME_KEY, "");
        final String userEmail = getArguments().getString(USER_EMAIL_KEY, "");
        final int userRating = getArguments().getInt(USER_RATING_KEY, 0);

        final View view = getActivity().getLayoutInflater().inflate(R.layout.admin_user_dialog, null);
        final EditText usernameEditText = (EditText) view.findViewById(R.id.user_name);
        final EditText userRatingEditText = (EditText) view.findViewById(R.id.user_rating);
        final TextView userEmailTextView = (TextView) view.findViewById(R.id.user_email);

        userEmailTextView.setText(getString(R.string.admin_user_email, userEmail));
        usernameEditText.setText(username);
        userRatingEditText.setText(String.valueOf(userRating));

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setTitle(R.string.admin_user_dialog_title)
                .setNegativeButton(R.string.about_close, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton(R.string.dialog_update, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((UserDialogListener) getTargetFragment()).onUserUpdateDialogSubmit(userEmail,
                                usernameEditText.getText().toString(), userRatingEditText.getText().toString());
                        dialogInterface.dismiss();
                    }
                })
                .create();
    }

    public interface UserDialogListener {
        void onUserUpdateDialogSubmit(String userEmail, String username, String rating);
    }
}
