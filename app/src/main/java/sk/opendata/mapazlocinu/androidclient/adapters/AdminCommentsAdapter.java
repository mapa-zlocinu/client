/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.adapters;

import android.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.model.comment.CommentAdmin;
import sk.opendata.mapazlocinu.androidclient.utils.IncidentsUtils;
import sk.opendata.mapazlocinu.androidclient.view.admin.AdminCommentsFragment;
import sk.opendata.mapazlocinu.androidclient.view.admindetail.AdminIncidentCommentsFragment;

public class AdminCommentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Fragment fragment;
    private final boolean showVisibility;
    private List<CommentAdmin> comments;

    public AdminCommentsAdapter(List<CommentAdmin> comments, Fragment fragment, boolean showVisibility) {
        this.comments = comments != null ? comments : new ArrayList<CommentAdmin>();
        this.fragment = fragment;
        this.showVisibility = showVisibility;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comments_admin_list_item, parent, false);
        return new CommentItemHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CommentItemHolder itemHolder = (CommentItemHolder) holder;

        final CommentAdmin item = comments.get(position);
        if (item.getAuthor() != null) {
            itemHolder.commentAuthor.setText(item.getAuthor().getName());
            itemHolder.authorEmail.setText(item.getAuthor().getEmail());
        } else {
            itemHolder.commentAuthor.setText("");
            itemHolder.authorEmail.setText("");
        }
        itemHolder.authorAnonymous.setVisibility(!item.isAuthorVisible() ? View.VISIBLE : View.GONE);
        itemHolder.commentDate.setText(IncidentsUtils.toFormattedDate(item.getCreatedDate()));
        itemHolder.commentType.setText(IncidentsUtils.commentTypeToStringResource(item.getType()));
        itemHolder.commentText.setText(item.getText());
        if (showVisibility) {
            itemHolder.commentVisibility.setVisibility(View.VISIBLE);
            itemHolder.commentVisibility.setText(IncidentsUtils.visibilityToStringResource(item.getVisibility()));
        } else {
            itemHolder.commentVisibility.setVisibility(View.GONE);
        }

        itemHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fragment instanceof AdminCommentsFragment) {
                    ((AdminCommentsFragment) fragment).showSmallDialog(item);
                } else if (fragment instanceof AdminIncidentCommentsFragment) {
                    ((AdminIncidentCommentsFragment) fragment).showSmallDialog(item);
                }
            }
        });

        itemHolder.view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (fragment instanceof AdminCommentsFragment) {
                    ((AdminCommentsFragment) fragment).showBigDialog(item);
                } else if (fragment instanceof AdminIncidentCommentsFragment) {
                    ((AdminIncidentCommentsFragment) fragment).showBigDialog(item);
                }
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.comments.size();
    }

    public void addNewDataSet(List<CommentAdmin> comments) {
        this.comments = comments != null ? comments : new ArrayList<CommentAdmin>();
        notifyDataSetChanged();
    }

    private class CommentItemHolder extends RecyclerView.ViewHolder {
        final View view;
        final TextView commentAuthor;
        final TextView authorEmail;
        final TextView authorAnonymous;
        final TextView commentDate;
        final TextView commentType;
        final TextView commentText;
        final TextView commentVisibility;

        CommentItemHolder(View itemView) {
            super(itemView);

            this.view = itemView;
            this.commentAuthor = (TextView) view.findViewById(R.id.comment_author);
            this.authorEmail = (TextView) view.findViewById(R.id.comment_author_email);
            this.authorAnonymous = (TextView) view.findViewById(R.id.comment_author_anonym);
            this.commentDate = (TextView) view.findViewById(R.id.comment_date);
            this.commentType = (TextView) view.findViewById(R.id.comment_type);
            this.commentText = (TextView) view.findViewById(R.id.comment_text);
            this.commentVisibility = (TextView) view.findViewById(R.id.comment_visibility);
        }
    }
}
