/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.data.MainActivityDataHolder;

public class IncidentFilterDialog extends DialogFragment {
    private FilterDialogListener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final CharSequence[] items = MainActivityDataHolder.getInstance().getFilterItems(getActivity());
        final int index = MainActivityDataHolder.getInstance().getFilterItemIndex();
        final int[] chosenIndex = {0};

        builder.setTitle(R.string.fialter_dialog_title)
                .setIcon(R.mipmap.ic_launcher)
                .setSingleChoiceItems(items, index, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        chosenIndex[0] = i;
                    }
                })
                .setNegativeButton(R.string.about_close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(R.string.dialog_submit_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        MainActivityDataHolder.getInstance().setFilterItemIndex(chosenIndex[0]);
                        listener.onFilterOptionSelected();
                        dialog.dismiss();
                    }
                });

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (FilterDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement FilterDialogListener!");
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (FilterDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement FilterDialogListener!");
        }
    }

    public interface FilterDialogListener {
        void onFilterOptionSelected();
    }
}
