/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import sk.opendata.mapazlocinu.androidclient.data.MainActivityDataHolder;
import sk.opendata.mapazlocinu.androidclient.dialogs.AboutDialog;
import sk.opendata.mapazlocinu.androidclient.dialogs.IncidentFilterDialog;
import sk.opendata.mapazlocinu.androidclient.view.main.MainActivityListFragment;
import sk.opendata.mapazlocinu.androidclient.view.main.MainActivityMapFragment;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, IncidentFilterDialog.FilterDialogListener {
    private static final String FRAGMENT_MAP_TAG = "main.fragment.map";
    private static final String FRAGMENT_LIST_TAG = "main.fragment.list";

    private String currentFragmentsTag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CreateIncidentActivity.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // start last selected view
        int lastViewType = MainActivityDataHolder.getInstance().getMainActivityActiveView();
        changeFragmentView(lastViewType);
        if (lastViewType == 1)
            navigationView.setCheckedItem(R.id.nav_incidents_map);
        else if (lastViewType == 2)
            navigationView.setCheckedItem(R.id.nav_incidents_heatmap);
        else
            navigationView.setCheckedItem(R.id.nav_incidents_list);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                MainActivityDataHolder.getInstance().setSearchQuery(query);
                refreshFragmentView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // do nothing
                return false;
            }
        });

        MenuItem searchMenuItem = menu.findItem(R.id.search);
        MenuItemCompat.setOnActionExpandListener(searchMenuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // do nothing
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                MainActivityDataHolder.getInstance().setSearchQuery(null);
                refreshFragmentView();
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_incidents_map && MainActivityDataHolder.getInstance().getMainActivityActiveView() != 1) {
            changeFragmentView(1);
        } else if (id == R.id.nav_incidents_heatmap && MainActivityDataHolder.getInstance().getMainActivityActiveView() != 2) {
            changeFragmentView(2);
        } else if (id == R.id.nav_incidents_list && MainActivityDataHolder.getInstance().getMainActivityActiveView() != 3) {
            changeFragmentView(3);
        } else if (id == R.id.nav_incidents_filter) {
            new IncidentFilterDialog().show(getFragmentManager(), "IncidentFilterDialog");
        } else if (id == R.id.nav_main_settings) {
            Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_main_about) {
            new AboutDialog().show(getFragmentManager(), "AboutDialog");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void changeFragmentView(int type) {
        Fragment fragment;
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if (type == 1) {
            currentFragmentsTag = FRAGMENT_MAP_TAG;
            fragment = MainActivityMapFragment.newInstance(MainActivityMapFragment.MAP_TYPE_NORMAL);
            MainActivityDataHolder.getInstance().setMainActivityActiveView(1);
        } else if (type == 2) {
            currentFragmentsTag = FRAGMENT_MAP_TAG;
            fragment = MainActivityMapFragment.newInstance(MainActivityMapFragment.MAP_TYPE_HEAT);
            MainActivityDataHolder.getInstance().setMainActivityActiveView(2);
        } else {
            currentFragmentsTag = FRAGMENT_LIST_TAG;
            fragment = MainActivityListFragment.newInstance();
            MainActivityDataHolder.getInstance().setMainActivityActiveView(3);
        }

        fragmentTransaction.replace(R.id.main_activity_fragment, fragment, currentFragmentsTag);
        fragmentTransaction.commit();
    }

    private void refreshFragmentView() {
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(currentFragmentsTag);

        if (fragment instanceof MainActivityMapFragment) {
            ((MainActivityMapFragment) fragment).refreshData();
        } else if (fragment instanceof MainActivityListFragment) {
            ((MainActivityListFragment) fragment).refreshData();
        }
    }

    @Override
    public void onFilterOptionSelected() {
        refreshFragmentView();
    }
}
