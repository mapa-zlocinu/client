/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.mzelzoghbi.zgallery.ZGallery;
import com.mzelzoghbi.zgallery.entities.ZColor;

import java.util.ArrayList;
import java.util.List;

import sk.opendata.mapazlocinu.androidclient.adapters.PublicIncidentPagerAdapter;
import sk.opendata.mapazlocinu.androidclient.api.ApiPublicController;
import sk.opendata.mapazlocinu.androidclient.api.listeners.CommentCreateListener;
import sk.opendata.mapazlocinu.androidclient.api.listeners.PublicAttachmentsLoadListener;
import sk.opendata.mapazlocinu.androidclient.dialogs.CreateCommentDialog;
import sk.opendata.mapazlocinu.androidclient.model.CreateItemResponse;
import sk.opendata.mapazlocinu.androidclient.model.attachment.AttachmentPublic;
import sk.opendata.mapazlocinu.androidclient.model.comment.CommentCreate;
import sk.opendata.mapazlocinu.androidclient.model.enums.Visibility;
import sk.opendata.mapazlocinu.androidclient.utils.IncidentsUtils;

public class PublicIncidentDetailActivity extends AppCompatActivity implements PublicAttachmentsLoadListener,
        CreateCommentDialog.CreateCommentListener, CommentCreateListener {
    public static final String INCIDENT_ID_ARG = "public.incident.id";

    private FloatingActionButton fab;
    private FloatingActionButton fab2;

    private int tabCurrentPosition = 0;
    private String incidentId;
    private ArrayList<String> attachmentsUrls = new ArrayList<>();

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_public_incident_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        this.incidentId = getIntent().getStringExtra(INCIDENT_ID_ARG);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeAttachmentsFabClick();
            }
        });
        fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeCommentsFabClick();
            }
        });

        PublicIncidentPagerAdapter pagerAdapter = new PublicIncidentPagerAdapter(getFragmentManager(), getApplicationContext());
        ViewPager mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(pagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabCurrentPosition = tab.getPosition();
                changeFab();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                // do nothing
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                // do nothing
            }
        });

        openProgressDialog();
        ApiPublicController.getInstance().registerListener((PublicAttachmentsLoadListener) this);
        ApiPublicController.getInstance().loadPublicIncidentsAttachments(this.incidentId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public String getIncidentId() {
        return incidentId;
    }

    @Override
    public void onIncidentAttachmentsLoad(List<AttachmentPublic> attachments) {
        this.attachmentsUrls.clear();
        for (AttachmentPublic attachment : attachments) {
            String link = IncidentsUtils.getAttachmentDataLink(attachment);
            if (link != null) {
                this.attachmentsUrls.add(link);
            }
        }

        changeFab();
        closeProgressDialog();
    }

    private void changeFab() {
        switch (tabCurrentPosition) {
            case 0:
                if (attachmentsUrls.isEmpty()) {
                    fab.hide();
                } else {
                    fab.show();
                }
                fab2.hide();
                break;
            case 1:
                fab.hide();
                fab2.show();
        }
    }

    private void executeAttachmentsFabClick() {
        if (attachmentsUrls.isEmpty())
            return;

        ZGallery.with(this, attachmentsUrls)
                .setTitle(getString(R.string.gallery_title))
                .setToolbarTitleColor(ZColor.WHITE)
                .setGalleryBackgroundColor(ZColor.WHITE)
                .setToolbarColorResId(R.color.colorAccent)
                .show();
    }

    private void executeCommentsFabClick() {
        new CreateCommentDialog().show(getFragmentManager(), "CreateCommentDialog");
    }

    private void openProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(getString(R.string.progress_dialog_title));
        progressDialog.setMessage(getString(R.string.progress_dialog_message));
        progressDialog.show();
    }

    private void closeProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onCreateCommentSubmit(CommentCreate commentCreate) {
        ApiPublicController.getInstance().registerListener((CommentCreateListener) this);
        ApiPublicController.getInstance().createComment(incidentId, commentCreate);
    }

    @Override
    public void onCommentCreated(CreateItemResponse createItemResponse) {
        if (createItemResponse.getItemStatus().equals(Visibility.APPROVED)) {
            Toast.makeText(getApplicationContext(), R.string.create_ok_approved, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), R.string.create_ok_pending, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onCommentCreateFailure() {
        Toast.makeText(getApplicationContext(), R.string.create_error, Toast.LENGTH_LONG).show();
    }
}
