/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.api.interfaces;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import sk.opendata.mapazlocinu.androidclient.model.admin.DataSummary;
import sk.opendata.mapazlocinu.androidclient.model.attachment.AttachmentAdmin;
import sk.opendata.mapazlocinu.androidclient.model.comment.CommentAdmin;
import sk.opendata.mapazlocinu.androidclient.model.comment.CommentUpdate;
import sk.opendata.mapazlocinu.androidclient.model.enums.Visibility;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentAdmin;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentAdminExcerpt;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentUpdate;
import sk.opendata.mapazlocinu.androidclient.model.security.LoginRequest;
import sk.opendata.mapazlocinu.androidclient.model.security.LoginResponse;
import sk.opendata.mapazlocinu.androidclient.model.user.UserAdmin;
import sk.opendata.mapazlocinu.androidclient.model.user.UserAdminUpdate;

public interface AdminApiInterface {
    // --- COMMON API ---
    @POST("admin/auth")
    Call<LoginResponse> postLoginRequest(@Body LoginRequest loginRequest);

    @GET("admin/summary")
    Call<DataSummary> getDataSummary(@Header("Authorization") String authorization);

    // --- USERS API ---
    @GET("admin/users")
    Call<List<UserAdmin>> getUsersList(@Header("Authorization") String authorization, @Query("maxSize") int maxSize,
                                       @Query("sortBy") String sortBy, @Query("sortType") String sortType);

    @PATCH("admin/users/{userEmail}/")
    Call<UserAdmin> patchUser(@Header("Authorization") String authorization, @Path("userEmail") String userEmail,
                              @Body UserAdminUpdate userAdminUpdate);

    // --- COMMENTS API ---
    @GET("admin/comments/pending")
    Call<List<CommentAdmin>> getPendingComments(@Header("Authorization") String authorization, @Query("maxSize") int maxSize,
                                                @Query("sortBy") String sortBy, @Query("sortType") String sortType);

    @GET("admin/comments/disapproved")
    Call<List<CommentAdmin>> getDisapprovedComments(@Header("Authorization") String authorization, @Query("maxSize") int maxSize,
                                                    @Query("sortBy") String sortBy, @Query("sortType") String sortType);

    @PATCH("admin/comments/{commentId}")
    Call<CommentAdmin> patchComment(@Header("Authorization") String authorization, @Path("commentId") String commentId,
                                    @Body CommentUpdate commentUpdate);

    @PATCH("admin/comments/{commentId}/{visibility}")
    Call<CommentAdmin> patchCommentVisibility(@Header("Authorization") String authorization, @Path("commentId") String commentId,
                                              @Path("visibility") Visibility visibility);

    @DELETE("admin/comments/{commentId}")
    Call<Void> deleteComment(@Header("Authorization") String authorization, @Path("commentId") String commentId);

    @GET("admin/comments/{commentId}/incident")
    Call<IncidentAdmin> getCommentsIncident(@Header("Authorization") String authorization, @Path("commentId") String commentId);

    // --- INCIDENTS API ---
    @GET("admin/incidents/pending")
    Call<List<IncidentAdminExcerpt>> getPendingIncidents(@Header("Authorization") String authorization, @Query("maxSize") int maxSize,
                                                         @Query("sortBy") String sortBy, @Query("sortType") String sortType);

    @GET("admin/incidents/disapproved")
    Call<List<IncidentAdminExcerpt>> getDisapprovedIncidents(@Header("Authorization") String authorization, @Query("maxSize") int maxSize,
                                                             @Query("sortBy") String sortBy, @Query("sortType") String sortType);

    @GET("admin/incidents/{incidentId}")
    Call<IncidentAdmin> getIncidentDetail(@Header("Authorization") String authorization, @Path("incidentId") String incidentId);

    @PATCH("admin/incidents/{incidentId}/{visibility}")
    Call<IncidentAdmin> patchIncidentVisibility(@Header("Authorization") String authorization, @Path("incidentId") String incidentId,
                                                @Path("visibility") Visibility visibility);

    @PATCH("admin/incidents/{incidentId}")
    Call<IncidentAdmin> patchIncident(@Header("Authorization") String authorization, @Path("incidentId") String incidentId,
                                      @Body IncidentUpdate incidentUpdate);

    @DELETE("admin/incidents/{incidentId}")
    Call<Void> deleteIncident(@Header("Authorization") String authorization, @Path("incidentId") String incidentId);

    @GET("admin/incidents/{incidentId}/comments")
    Call<List<CommentAdmin>> getIncidentComments(@Header("Authorization") String authorization, @Path("incidentId") String incidentId,
                                                 @Query("maxSize") int maxSize, @Query("sortBy") String sortBy, @Query("sortType") String sortType);

    @GET("admin/incidents/{incidentId}/attachments")
    Call<List<AttachmentAdmin>> getIncidentAttachments(@Header("Authorization") String authorization, @Path("incidentId") String incidentId,
                                                       @Query("maxSize") int maxSize, @Query("sortBy") String sortBy, @Query("sortType") String sortType);

    // --- ATTACHMENTS API ---
    @DELETE("admin/attachments/{attachmentId}")
    Call<Void> deleteAttachment(@Header("Authorization") String authorization, @Path("attachmentId") String attachmentId);
}
