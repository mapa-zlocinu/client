/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sk.opendata.mapazlocinu.androidclient.PublicIncidentDetailActivity;
import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentPublicExcerpt;
import sk.opendata.mapazlocinu.androidclient.utils.IncidentsUtils;

public class IncidentsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context context;
    private List<IncidentPublicExcerpt> incidents;

    public IncidentsListAdapter(List<IncidentPublicExcerpt> incidents, Context context) {
        this.incidents = incidents != null ? incidents : new ArrayList<IncidentPublicExcerpt>();
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.incident_list_item, parent, false);
        return new IncidentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        IncidentViewHolder viewHolder = (IncidentViewHolder) holder;

        final IncidentPublicExcerpt incident = this.incidents.get(position);
        viewHolder.incidentTitle.setText(incident.getTitle());
        viewHolder.incidentType.setText(IncidentsUtils.incidentTypeToStringResource(incident));
        viewHolder.incidentDate.setText(IncidentsUtils.toFormattedDate(incident.getIncidentDate()));

        viewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PublicIncidentDetailActivity.class);
                intent.putExtra(PublicIncidentDetailActivity.INCIDENT_ID_ARG, incident.getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.incidents.size();
    }

    public void addNewDataSet(List<IncidentPublicExcerpt> incidents) {
        this.incidents = incidents != null ? incidents : new ArrayList<IncidentPublicExcerpt>();
        notifyDataSetChanged();
    }

    private class IncidentViewHolder extends RecyclerView.ViewHolder {
        final View view;
        final TextView incidentTitle;
        final TextView incidentType;
        final TextView incidentDate;

        IncidentViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;
            this.incidentTitle = (TextView) view.findViewById(R.id.incident_item_title);
            this.incidentType = (TextView) view.findViewById(R.id.incident_item_type);
            this.incidentDate = (TextView) view.findViewById(R.id.incident_item_date);
        }
    }
}
