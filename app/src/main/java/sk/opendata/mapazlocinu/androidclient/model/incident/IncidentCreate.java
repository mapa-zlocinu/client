/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.model.incident;

import sk.opendata.mapazlocinu.androidclient.model.enums.IncidentType;
import sk.opendata.mapazlocinu.androidclient.model.user.UserPublic;

public class IncidentCreate {
    private String title;
    private String description;
    private long incidentDate;
    private UserPublic author;
    private boolean authorVisible;
    private double lat;
    private double lon;
    private String locationAddress;
    private IncidentType type;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getIncidentDate() {
        return incidentDate;
    }

    public void setIncidentDate(long incidentDate) {
        this.incidentDate = incidentDate;
    }

    public UserPublic getAuthor() {
        return author;
    }

    public void setAuthor(UserPublic author) {
        this.author = author;
    }

    public boolean isAuthorVisible() {
        return authorVisible;
    }

    public void setAuthorVisible(boolean authorVisible) {
        this.authorVisible = authorVisible;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }

    public IncidentType getType() {
        return type;
    }

    public void setType(IncidentType type) {
        this.type = type;
    }
}
