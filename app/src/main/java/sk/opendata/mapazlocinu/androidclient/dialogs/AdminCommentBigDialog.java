/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.model.comment.CommentAdmin;
import sk.opendata.mapazlocinu.androidclient.model.enums.CommentType;
import sk.opendata.mapazlocinu.androidclient.model.enums.Visibility;

public class AdminCommentBigDialog extends DialogFragment {
    private static final String COMMENT_ID_KEY = "AdminCommentBigDialog.key";
    private static final String COMMENT_TEXT_KEY = "AdminCommentBigDialog.text";
    private static final String COMMENT_AUTHOR_KEY = "AdminCommentBigDialog.author";
    private static final String COMMENT_TYPE_KEY = "AdminCommentBigDialog.type";
    private static final String COMMENT_DATE_KEY = "AdminCommentBigDialog.date";
    private static final String COMMENT_VISIBILITY_KEY = "AdminCommentBigDialog.visibility";

    public static AdminCommentBigDialog newInstance(CommentAdmin comment) {
        AdminCommentBigDialog dialog = new AdminCommentBigDialog();

        Bundle bundle = new Bundle();
        bundle.putString(COMMENT_ID_KEY, comment.getId());
        bundle.putString(COMMENT_TEXT_KEY, comment.getText());
        bundle.putString(COMMENT_TYPE_KEY, comment.getType().name());
        bundle.putBoolean(COMMENT_AUTHOR_KEY, comment.isAuthorVisible());
        bundle.putLong(COMMENT_DATE_KEY, comment.getCreatedDate());
        bundle.putString(COMMENT_VISIBILITY_KEY, comment.getVisibility().name());
        dialog.setArguments(bundle);

        return dialog;
    }

    @SuppressWarnings("InflateParams")
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final String commentId = getArguments().getString(COMMENT_ID_KEY);
        final String commentText = getArguments().getString(COMMENT_TEXT_KEY);
        final CommentType commentType = CommentType.valueOf(getArguments().getString(COMMENT_TYPE_KEY));
        final boolean authorVisible = getArguments().getBoolean(COMMENT_AUTHOR_KEY);
        final long createdDate = getArguments().getLong(COMMENT_DATE_KEY);
        final Visibility visibility = Visibility.valueOf(getArguments().getString(COMMENT_VISIBILITY_KEY));

        final View view = getActivity().getLayoutInflater().inflate(R.layout.admin_comment_dialog, null);
        final EditText commentTextEditText = (EditText) view.findViewById(R.id.comment_text);
        final CheckBox authorAnonymousCheckbox = (CheckBox) view.findViewById(R.id.author_visible);
        final Spinner commentTypeSpinner = (Spinner) view.findViewById(R.id.comment_type);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.comments_type, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        commentTypeSpinner.setAdapter(adapter);
        if (commentType.equals(CommentType.CONFIRMATION)) {
            commentTypeSpinner.setSelection(0);
        } else if (commentType.equals(CommentType.REFUTATION)) {
            commentTypeSpinner.setSelection(1);
        } else {
            commentTypeSpinner.setSelection(2);
        }

        authorAnonymousCheckbox.setChecked(!authorVisible);
        commentTextEditText.setText(commentText);

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setTitle(R.string.comment_update)
                .setNeutralButton(R.string.about_close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(R.string.dialog_update2, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String newText = commentTextEditText.getText().toString();
                        boolean newAuthorVisible = !authorAnonymousCheckbox.isChecked();
                        int selectedTypePos = commentTypeSpinner.getSelectedItemPosition();
                        CommentType newType;
                        if (selectedTypePos == 0) {
                            newType = CommentType.CONFIRMATION;
                        } else if (selectedTypePos == 1) {
                            newType = CommentType.REFUTATION;
                        } else {
                            newType = CommentType.NEUTRAL;
                        }

                        ((AdminCommentBigDialogListener) getTargetFragment()).onCommentUpdateRequest(commentId, newText, newType,
                                newAuthorVisible, visibility, createdDate);
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton(R.string.dialog_delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((AdminCommentBigDialogListener) getTargetFragment()).onCommentDeleteRequest(commentId);
                        dialogInterface.dismiss();
                    }
                })
                .create();
    }

    public interface AdminCommentBigDialogListener {
        void onCommentDeleteRequest(String commentId);

        void onCommentUpdateRequest(String commentId, String commentText, CommentType commentType, boolean authorVisible,
                                    Visibility visibility, long createdDate);
    }
}
