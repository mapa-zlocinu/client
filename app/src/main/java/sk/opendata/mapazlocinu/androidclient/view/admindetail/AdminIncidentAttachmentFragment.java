/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.view.admindetail;


import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import sk.opendata.mapazlocinu.androidclient.AdminIncidentDetailActivity;
import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.adapters.AdminAttachmentsAdapter;
import sk.opendata.mapazlocinu.androidclient.api.ApiAdminController;
import sk.opendata.mapazlocinu.androidclient.api.listeners.admin.AdminIncidentAttachmentsListener;
import sk.opendata.mapazlocinu.androidclient.dialogs.AdminAttachmentDialog;
import sk.opendata.mapazlocinu.androidclient.dividers.ItemDecorationDivider;
import sk.opendata.mapazlocinu.androidclient.model.attachment.AttachmentAdmin;

public class AdminIncidentAttachmentFragment extends Fragment implements AdminIncidentAttachmentsListener, AdminAttachmentDialog.AdminAttachmentDialogListener {
    private SwipeRefreshLayout swipeRefreshLayout;
    private AdminAttachmentsAdapter adapter;
    private TextView noDataTextView;

    private String incidentId;

    public AdminIncidentAttachmentFragment() {
    }

    public static Fragment newInstance() {
        return new AdminIncidentAttachmentFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin_incident_attachment, container, false);

        this.incidentId = ((AdminIncidentDetailActivity) getActivity()).getIncidentId();
        initView(view);

        ApiAdminController.getInstance().registerListener(this);
        loadAttachments();

        return view;
    }

    private void initView(View view) {
        noDataTextView = (TextView) view.findViewById(R.id.no_data_text);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.attachment_admin_detail_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new AdminAttachmentsAdapter(new ArrayList<AttachmentAdmin>(), getActivity(), this);

        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new ItemDecorationDivider(getActivity()));

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.attachments_admin_detail_recycler_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadAttachments();
            }
        });
    }

    private void loadAttachments() {
        ApiAdminController.getInstance().getIncidentsAttachments(getActivity(), this.incidentId);
    }

    private void stopRefreshing() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onAttachmentsLoad(List<AttachmentAdmin> attachments) {
        if (attachments == null || attachments.isEmpty()) {
            noDataTextView.setVisibility(View.VISIBLE);
        } else {
            noDataTextView.setVisibility(View.GONE);
        }

        adapter.addNewDataSet(attachments);
        stopRefreshing();
    }

    @Override
    public void onRequestError() {
        Toast.makeText(getActivity(), R.string.admin_data_fail, Toast.LENGTH_LONG).show();
        stopRefreshing();
    }

    @Override
    public void onRequestSuccess() {
        loadAttachments();
        Toast.makeText(getActivity(), R.string.operation_done, Toast.LENGTH_LONG).show();
        stopRefreshing();
    }

    public void showAttachmentDialog(String attachmentId, String attachmentTitle) {
        AdminAttachmentDialog dialog = AdminAttachmentDialog.newInstance(attachmentId, attachmentTitle);
        dialog.setTargetFragment(this, 251);
        dialog.show(getChildFragmentManager(), "AdminAttachmentDialog");
    }

    @Override
    public void onAttachmentDeleteRequest(String attachmentId) {
        ApiAdminController.getInstance().deleteAttachment(getActivity(), attachmentId);
    }
}
