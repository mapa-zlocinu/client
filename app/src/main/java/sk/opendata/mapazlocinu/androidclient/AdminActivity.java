/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import sk.opendata.mapazlocinu.androidclient.api.ApiAdminController;
import sk.opendata.mapazlocinu.androidclient.api.listeners.admin.DataSummaryLoadListener;
import sk.opendata.mapazlocinu.androidclient.data.AdminActivityDataHolder;
import sk.opendata.mapazlocinu.androidclient.dialogs.AdminSummaryDialog;
import sk.opendata.mapazlocinu.androidclient.model.admin.DataSummary;
import sk.opendata.mapazlocinu.androidclient.utils.AuthUtils;
import sk.opendata.mapazlocinu.androidclient.view.admin.AdminCommentsFragment;
import sk.opendata.mapazlocinu.androidclient.view.admin.AdminIncidentsFragment;
import sk.opendata.mapazlocinu.androidclient.view.admin.AdminUsersFragment;

public class AdminActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, DataSummaryLoadListener {
    private final static String FRAGMENT_INCIDENTS_TAG = "admin.fragment.incidents";
    private final static String FRAGMENT_COMMENTS_TAG = "admin.fragment.comments";
    private final static String FRAGMENT_USERS_TAG = "admin.fragment.users";

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        ((TextView) navigationView.getHeaderView(0).findViewById(R.id.optional_text_view))
                .setText(getString(R.string.logged_in_admin, AuthUtils.getAuthName(getApplicationContext())));

        int lastSection = AdminActivityDataHolder.getInstance().getAdminActivityActiveSection();
        if (lastSection == 1) {
            changeFragmentView(1);
            navigationView.setCheckedItem(R.id.nav_admin_incidents);
        } else if (lastSection == 2) {
            changeFragmentView(2);
            navigationView.setCheckedItem(R.id.nav_admin_comments);
        } else {
            changeFragmentView(3);
            navigationView.setCheckedItem(R.id.nav_admin_users);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            AuthUtils.deleteAuthDetails(getApplicationContext());
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_admin_incidents && AdminActivityDataHolder.getInstance().getAdminActivityActiveSection() != 1) {
            changeFragmentView(1);
        } else if (id == R.id.nav_admin_comments && AdminActivityDataHolder.getInstance().getAdminActivityActiveSection() != 2) {
            changeFragmentView(2);
        } else if (id == R.id.nav_admin_users && AdminActivityDataHolder.getInstance().getAdminActivityActiveSection() != 3) {
            changeFragmentView(3);
        } else if (id == R.id.nav_admin_summary) {
            openProgressDialog();
            ApiAdminController.getInstance().registerListener(this);
            ApiAdminController.getInstance().getDataSummary(getApplicationContext());
        } else if (id == R.id.nav_admin_logout) {
            AuthUtils.deleteAuthDetails(getApplicationContext());
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onSummaryLoadFinish(DataSummary dataSummary) {
        closeProgressDialog();
        if (dataSummary != null) {
            AdminSummaryDialog adminSummaryDialog = AdminSummaryDialog.newInstance(dataSummary);
            adminSummaryDialog.show(getFragmentManager(), "AdminSummaryDialog");
        } else {
            Toast.makeText(getApplicationContext(), R.string.admin_data_fail, Toast.LENGTH_LONG).show();
        }
    }

    private void changeFragmentView(int type) {
        Fragment fragment;
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        int titleId;

        String currentFragmentsTag;
        if (type == 1) {
            titleId = R.string.screen_incidents;
            currentFragmentsTag = FRAGMENT_INCIDENTS_TAG;
            fragment = AdminIncidentsFragment.newInstance();
            AdminActivityDataHolder.getInstance().setAdminActivityActiveSection(1);
        } else if (type == 2) {
            titleId = R.string.screen_comments;
            currentFragmentsTag = FRAGMENT_COMMENTS_TAG;
            fragment = AdminCommentsFragment.newInstance();
            AdminActivityDataHolder.getInstance().setAdminActivityActiveSection(2);
        } else {
            titleId = R.string.screen_users;
            currentFragmentsTag = FRAGMENT_USERS_TAG;
            fragment = AdminUsersFragment.newInstance();
            AdminActivityDataHolder.getInstance().setAdminActivityActiveSection(3);
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(titleId);
        }

        fragmentTransaction.replace(R.id.admin_activity_fragment, fragment, currentFragmentsTag);
        fragmentTransaction.commit();
    }

    private void openProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(getString(R.string.progress_dialog_title));
        progressDialog.setMessage(getString(R.string.progress_dialog_message));
        progressDialog.show();
    }

    private void closeProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
