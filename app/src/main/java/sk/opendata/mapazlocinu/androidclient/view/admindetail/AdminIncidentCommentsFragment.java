/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.view.admindetail;


import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import sk.opendata.mapazlocinu.androidclient.AdminIncidentDetailActivity;
import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.adapters.AdminCommentsAdapter;
import sk.opendata.mapazlocinu.androidclient.api.ApiAdminController;
import sk.opendata.mapazlocinu.androidclient.api.listeners.admin.AdminCommentsListener;
import sk.opendata.mapazlocinu.androidclient.api.listeners.admin.AdminIncidentCommentsListener;
import sk.opendata.mapazlocinu.androidclient.dialogs.AdminCommentBigDialog;
import sk.opendata.mapazlocinu.androidclient.dialogs.AdminCommentSmallDialog;
import sk.opendata.mapazlocinu.androidclient.dividers.ItemDecorationDivider;
import sk.opendata.mapazlocinu.androidclient.model.comment.CommentAdmin;
import sk.opendata.mapazlocinu.androidclient.model.comment.CommentUpdate;
import sk.opendata.mapazlocinu.androidclient.model.enums.CommentType;
import sk.opendata.mapazlocinu.androidclient.model.enums.Visibility;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentAdmin;

public class AdminIncidentCommentsFragment extends Fragment implements AdminIncidentCommentsListener, AdminCommentsListener,
        AdminCommentSmallDialog.AdminCommentSmallDialogListener, AdminCommentBigDialog.AdminCommentBigDialogListener {
    private SwipeRefreshLayout swipeRefreshLayout;
    private AdminCommentsAdapter adapter;
    private TextView noDataTextView;

    private String incidentId;

    public AdminIncidentCommentsFragment() {
    }

    public static Fragment newInstance() {
        return new AdminIncidentCommentsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin_incident_comments, container, false);

        this.incidentId = ((AdminIncidentDetailActivity) getActivity()).getIncidentId();
        initView(view);
        ApiAdminController.getInstance().registerListener((AdminIncidentCommentsListener) this);
        ApiAdminController.getInstance().registerListener((AdminCommentsListener) this);
        loadCommentsData();

        return view;
    }

    private void initView(View view) {
        noDataTextView = (TextView) view.findViewById(R.id.no_data_text);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.comments_admin_detail_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new AdminCommentsAdapter(new ArrayList<CommentAdmin>(), this, true);

        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new ItemDecorationDivider(getActivity()));

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.comments_admin_detail_recycler_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadCommentsData();
            }
        });
    }

    @Override
    public void onCommentsLoad(List<CommentAdmin> comments) {
        if (comments == null || comments.isEmpty()) {
            noDataTextView.setVisibility(View.VISIBLE);
        } else {
            noDataTextView.setVisibility(View.GONE);
        }

        adapter.addNewDataSet(comments);
        stopRefreshing();
    }

    @Override
    public void onCommentsLoadError() {
        Toast.makeText(getActivity(), R.string.admin_data_fail, Toast.LENGTH_LONG).show();
        stopRefreshing();
    }

    private void stopRefreshing() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void loadCommentsData() {
        ApiAdminController.getInstance().getIncidentsComments(getActivity(), incidentId);
    }

    public void showSmallDialog(CommentAdmin comment) {
        AdminCommentSmallDialog dialog = AdminCommentSmallDialog.newInstance(comment, false);
        dialog.setTargetFragment(this, 250);
        dialog.show(getChildFragmentManager(), "AdminCommentSmallDialog");
    }

    public void showBigDialog(CommentAdmin comment) {
        AdminCommentBigDialog dialog = AdminCommentBigDialog.newInstance(comment);
        dialog.setTargetFragment(this, 251);
        dialog.show(getChildFragmentManager(), "AdminCommentBigDialog");
    }

    @Override
    public void onCommentVisibilityUpdateRequest(String commentId, Visibility visibility) {
        ApiAdminController.getInstance().updateCommentVisibility(getActivity(), commentId, visibility);
    }

    @Override
    public void onCommentIncidentShowRequest(String commentId) {
        // do nothing
    }

    @Override
    public void onCommentDeleteRequest(String commentId) {
        ApiAdminController.getInstance().deleteComment(getActivity(), commentId);
    }

    @Override
    public void onCommentUpdateRequest(String commentId, String commentText, CommentType commentType, boolean authorVisible, Visibility visibility, long createdDate) {
        if (commentText.length() < 5 || commentText.length() > 800) {
            Toast.makeText(getActivity(), R.string.validation_error, Toast.LENGTH_LONG).show();
            return;
        }

        CommentUpdate update = new CommentUpdate();
        update.setText(commentText);
        update.setType(commentType);
        update.setAuthorVisible(authorVisible);
        update.setVisibility(visibility);
        update.setCreatedDate(createdDate);

        ApiAdminController.getInstance().updateComment(getActivity(), commentId, update);
    }

    @Override
    public void onCommentsLoad(List<CommentAdmin> comments, Visibility visibility) {
        // do nothing
    }

    @Override
    public void onRequestError() {
        Toast.makeText(getActivity(), R.string.admin_data_fail, Toast.LENGTH_LONG).show();
        stopRefreshing();
    }

    @Override
    public void onRequestSuccess(CommentAdmin commentAdmin) {
        Toast.makeText(getActivity(), R.string.operation_done, Toast.LENGTH_LONG).show();
        loadCommentsData();
    }

    @Override
    public void onRequestSuccess() {
        Toast.makeText(getActivity(), R.string.operation_done, Toast.LENGTH_LONG).show();
        loadCommentsData();
    }

    @Override
    public void onCommentIncidentLoad(IncidentAdmin incidentAdmin) {
        // do nothing
    }
}
