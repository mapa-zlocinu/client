/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.data;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.model.enums.IncidentType;
import sk.opendata.mapazlocinu.androidclient.model.enums.Visibility;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentAdminExcerpt;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentPublicExcerpt;
import sk.opendata.mapazlocinu.androidclient.utils.IncidentsUtils;

public class MainActivityDataHolder {
    private static MainActivityDataHolder instance;

    private List<IncidentPublicExcerpt> publicIncidents;
    private int filterItemIndex = 0;    // FILTER, 0 == ALL, other numbers based on IncidentType.values()
    private String searchQuery = null;

    private int mainActivityActiveView = 1;

    private MainActivityDataHolder() {
    }

    public static MainActivityDataHolder getInstance() {
        if (instance == null) {
            instance = new MainActivityDataHolder();
        }

        return instance;
    }

    public List<IncidentPublicExcerpt> getPublicIncidents() {
        if (searchQuery != null) {
            List<IncidentPublicExcerpt> tmp = new ArrayList<>();

            for (IncidentPublicExcerpt incident : this.publicIncidents) {
                if (incident.getTitle().toLowerCase().contains(searchQuery.toLowerCase())) {
                    tmp.add(incident);
                }
            }

            return tmp;
        } else if (filterItemIndex == 0) {
            return publicIncidents;
        } else {
            IncidentType filteredType = IncidentType.values()[filterItemIndex - 1];
            List<IncidentPublicExcerpt> tmp = new ArrayList<>();

            for (IncidentPublicExcerpt incident : this.publicIncidents) {
                if (incident.getType().equals(filteredType)) {
                    tmp.add(incident);
                }
            }

            return tmp;
        }
    }

    public void setPublicIncidents(List<IncidentPublicExcerpt> publicIncidents) {
        this.filterItemIndex = 0;
        this.publicIncidents = publicIncidents;
    }

    public List<IncidentAdminExcerpt> toAdminIncidents() {
        List<IncidentAdminExcerpt> output = new ArrayList<>();

        for (IncidentPublicExcerpt incident : this.publicIncidents) {
            IncidentAdminExcerpt tmp = new IncidentAdminExcerpt();
            tmp.setId(incident.getId());
            tmp.setIncidentDate(incident.getIncidentDate());
            tmp.setLat(incident.getLat());
            tmp.setLon(incident.getLon());
            tmp.setTitle(incident.getTitle());
            tmp.setType(incident.getType());
            tmp.setVisibility(Visibility.APPROVED);

            output.add(tmp);
        }

        return output;
    }

    public int getFilterItemIndex() {
        return filterItemIndex;
    }

    public void setFilterItemIndex(int filterItemIndex) {
        this.filterItemIndex = filterItemIndex;
    }

    public CharSequence[] getFilterItems(Context context) {
        CharSequence[] filterItems = new CharSequence[IncidentType.values().length + 1];
        filterItems[0] = context.getString(R.string.incident_type_all);

        int index = 1;
        for (IncidentType type : IncidentType.values()) {
            filterItems[index] = context.getString(IncidentsUtils.incidentTypeToStringResource(type));
            index++;
        }

        return filterItems;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public int getMainActivityActiveView() {
        return mainActivityActiveView;
    }

    public void setMainActivityActiveView(int mainActivityActiveView) {
        this.mainActivityActiveView = mainActivityActiveView;
    }
}
