/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.view.admin;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import sk.opendata.mapazlocinu.androidclient.AdminIncidentDetailActivity;
import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.adapters.AdminCommentsAdapter;
import sk.opendata.mapazlocinu.androidclient.api.ApiAdminController;
import sk.opendata.mapazlocinu.androidclient.api.listeners.admin.AdminCommentsListener;
import sk.opendata.mapazlocinu.androidclient.dialogs.AdminCommentBigDialog;
import sk.opendata.mapazlocinu.androidclient.dialogs.AdminCommentSmallDialog;
import sk.opendata.mapazlocinu.androidclient.dividers.ItemDecorationDivider;
import sk.opendata.mapazlocinu.androidclient.model.comment.CommentAdmin;
import sk.opendata.mapazlocinu.androidclient.model.comment.CommentUpdate;
import sk.opendata.mapazlocinu.androidclient.model.enums.CommentType;
import sk.opendata.mapazlocinu.androidclient.model.enums.Visibility;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentAdmin;
import sk.opendata.mapazlocinu.androidclient.utils.Utils;

public class AdminCommentsFragment extends Fragment implements AdminCommentsListener, AdminCommentSmallDialog.AdminCommentSmallDialogListener,
        AdminCommentBigDialog.AdminCommentBigDialogListener {

    private SwipeRefreshLayout swipeRefreshLayout;
    private AdminCommentsAdapter adapter;
    private ProgressDialog progressDialog;
    private BottomNavigationView navigation;

    private List<CommentAdmin> pendingComments = new ArrayList<>();
    private List<CommentAdmin> disapprovedComments = new ArrayList<>();

    public AdminCommentsFragment() {
    }

    public static Fragment newInstance() {
        return new AdminCommentsFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin_comments, container, false);

        initView(view);
        progressDialog = new ProgressDialog(getActivity());
        Utils.openProgressDialog(progressDialog, getActivity());
        ApiAdminController.getInstance().registerListener(this);
        ApiAdminController.getInstance().getComments(getActivity(), Visibility.PENDING);

        return view;
    }

    public void showSmallDialog(CommentAdmin comment) {
        AdminCommentSmallDialog dialog = AdminCommentSmallDialog.newInstance(comment, true);
        dialog.setTargetFragment(this, 250);
        dialog.show(getChildFragmentManager(), "AdminCommentSmallDialog");
    }

    public void showBigDialog(CommentAdmin comment) {
        AdminCommentBigDialog dialog = AdminCommentBigDialog.newInstance(comment);
        dialog.setTargetFragment(this, 251);
        dialog.show(getChildFragmentManager(), "AdminCommentBigDialog");
    }

    private void initView(View view) {
        navigation = (BottomNavigationView) view.findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.com_nav_pending:
                        switchAdapter(1);
                        return true;
                    case R.id.com_nav_approved:
                        switchAdapter(2);
                        return true;
                }
                return false;
            }
        });

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.comments_admin_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new AdminCommentsAdapter(new ArrayList<CommentAdmin>(), this, false);

        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new ItemDecorationDivider(getActivity()));

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.comments_admin_recycler_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ApiAdminController.getInstance().getComments(getActivity(), Visibility.PENDING);
            }
        });
    }

    private void switchAdapter(int type) {
        if (type == 1) {
            adapter.addNewDataSet(this.pendingComments);
        } else {
            adapter.addNewDataSet(this.disapprovedComments);
        }
    }

    private void stopAllProgressItems() {
        Utils.closeProgressDialog(progressDialog);
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onCommentsLoad(List<CommentAdmin> comments, Visibility visibility) {
        if (visibility.equals(Visibility.PENDING)) {
            this.pendingComments = comments;
            ApiAdminController.getInstance().getComments(getActivity(), Visibility.DISAPPROVED);
        } else {
            this.disapprovedComments = comments;
            switchAdapter(1);
            navigation.setSelectedItemId(R.id.com_nav_pending);
            stopAllProgressItems();
        }
    }

    @Override
    public void onRequestError() {
        Toast.makeText(getActivity(), R.string.admin_data_fail, Toast.LENGTH_LONG).show();
        stopAllProgressItems();
    }

    @Override
    public void onRequestSuccess(CommentAdmin commentAdmin) {
        Toast.makeText(getActivity(), R.string.operation_done, Toast.LENGTH_LONG).show();
        Utils.openProgressDialog(progressDialog, getActivity());
        ApiAdminController.getInstance().getComments(getActivity(), Visibility.PENDING);
    }

    @Override
    public void onRequestSuccess() {
        Toast.makeText(getActivity(), R.string.operation_done, Toast.LENGTH_LONG).show();
        Utils.openProgressDialog(progressDialog, getActivity());
        ApiAdminController.getInstance().getComments(getActivity(), Visibility.PENDING);
    }

    @Override
    public void onCommentIncidentLoad(IncidentAdmin incidentAdmin) {
        Intent intent = new Intent(getActivity(), AdminIncidentDetailActivity.class);
        intent.putExtra(AdminIncidentDetailActivity.INCIDENT_OBJECT_KEY, incidentAdmin);
        intent.putExtra(AdminIncidentDetailActivity.INCIDENT_ID_KEY, incidentAdmin.getId());
        getActivity().startActivity(intent);
    }

    @Override
    public void onCommentVisibilityUpdateRequest(String commentId, Visibility visibility) {
        ApiAdminController.getInstance().updateCommentVisibility(getActivity(), commentId, visibility);
    }

    @Override
    public void onCommentIncidentShowRequest(String commentId) {
        ApiAdminController.getInstance().getCommentsIncident(getActivity(), commentId);
    }

    @Override
    public void onCommentDeleteRequest(String commentId) {
        ApiAdminController.getInstance().deleteComment(getActivity(), commentId);
    }

    @Override
    public void onCommentUpdateRequest(String commentId, String commentText, CommentType commentType, boolean authorVisible, Visibility visibility, long createdDate) {
        if (commentText.length() < 5 || commentText.length() > 800) {
            Toast.makeText(getActivity(), R.string.validation_error, Toast.LENGTH_LONG).show();
            return;
        }

        CommentUpdate update = new CommentUpdate();
        update.setText(commentText);
        update.setType(commentType);
        update.setAuthorVisible(authorVisible);
        update.setVisibility(visibility);
        update.setCreatedDate(createdDate);

        ApiAdminController.getInstance().updateComment(getActivity(), commentId, update);
    }
}
