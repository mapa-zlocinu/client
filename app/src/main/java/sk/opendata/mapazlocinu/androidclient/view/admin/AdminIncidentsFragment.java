/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.view.admin;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.adapters.AdminIncidentAdapter;
import sk.opendata.mapazlocinu.androidclient.api.ApiAdminController;
import sk.opendata.mapazlocinu.androidclient.api.listeners.admin.AdminIncidentsListener;
import sk.opendata.mapazlocinu.androidclient.data.MainActivityDataHolder;
import sk.opendata.mapazlocinu.androidclient.dividers.ItemDecorationDivider;
import sk.opendata.mapazlocinu.androidclient.model.enums.Visibility;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentAdminExcerpt;
import sk.opendata.mapazlocinu.androidclient.utils.Utils;

public class AdminIncidentsFragment extends Fragment implements AdminIncidentsListener {
    private AdminIncidentAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressDialog progressDialog;
    private BottomNavigationView navigation;

    private List<IncidentAdminExcerpt> approvedIncidents = new ArrayList<>();
    private List<IncidentAdminExcerpt> pendingIncidents = new ArrayList<>();
    private List<IncidentAdminExcerpt> disapprovedIncidents = new ArrayList<>();

    public AdminIncidentsFragment() {
    }

    public static Fragment newInstance() {
        return new AdminIncidentsFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin_incidents, container, false);

        initView(view);
        progressDialog = new ProgressDialog(getActivity());
        Utils.openProgressDialog(progressDialog, getActivity());

        approvedIncidents = MainActivityDataHolder.getInstance().toAdminIncidents();
        ApiAdminController.getInstance().registerListener(this);
        ApiAdminController.getInstance().getIncidents(getActivity(), Visibility.PENDING);

        return view;
    }

    private void initView(View view) {
        navigation = (BottomNavigationView) view.findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.inc_nav_pending:
                        switchAdapter(1);
                        return true;
                    case R.id.inc_nav_approved:
                        switchAdapter(2);
                        return true;
                    case R.id.inc_nav_disapproved:
                        switchAdapter(3);
                        return true;
                }
                return false;
            }
        });

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.incidents_admin_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new AdminIncidentAdapter(new ArrayList<IncidentAdminExcerpt>(), getActivity());

        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new ItemDecorationDivider(getActivity()));

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.incidents_admin_recycler_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ApiAdminController.getInstance().getIncidents(getActivity(), Visibility.PENDING);
            }
        });
    }

    private void stopAllProgressItems() {
        Utils.closeProgressDialog(progressDialog);
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void switchAdapter(int type) {
        if (type == 1) {
            adapter.addNewDataSet(this.pendingIncidents);
        } else if (type == 2) {
            adapter.addNewDataSet(this.approvedIncidents);
        } else {
            adapter.addNewDataSet(this.disapprovedIncidents);
        }
    }

    @Override
    public void onIncidentsLoad(List<IncidentAdminExcerpt> incidents, Visibility visibility) {
        if (visibility.equals(Visibility.PENDING)) {
            this.pendingIncidents = incidents;
            ApiAdminController.getInstance().getIncidents(getActivity(), Visibility.DISAPPROVED);
        } else {
            this.disapprovedIncidents = incidents;
            switchAdapter(1);
            navigation.setSelectedItemId(R.id.inc_nav_pending);
            stopAllProgressItems();
        }
    }

    @Override
    public void onRequestError() {
        Toast.makeText(getActivity(), R.string.admin_data_fail, Toast.LENGTH_LONG).show();
        stopAllProgressItems();
    }
}
