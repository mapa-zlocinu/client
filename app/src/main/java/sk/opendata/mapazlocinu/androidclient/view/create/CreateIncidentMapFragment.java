/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.view.create;


import android.Manifest;
import android.app.Fragment;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import sk.opendata.mapazlocinu.androidclient.CreateIncidentActivity;
import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.data.AddIncidentDataHolder;

public class CreateIncidentMapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerDragListener {
    public static final int MY_PERMISSION_REQUEST_LOCATION = 20;

    private GoogleMap map;
    private LatLng position;

    public CreateIncidentMapFragment() {
    }

    public static Fragment newInstance() {
        return new CreateIncidentMapFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_incident_map, container, false);

        view.findViewById(R.id.inc_position).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position != null) {
                    AddIncidentDataHolder.getInstance().setLat(position.latitude);
                    AddIncidentDataHolder.getInstance().setLon(position.longitude);

                    String address = getAddressFromGeocoder();
                    if (address != null) {
                        AddIncidentDataHolder.getInstance().setLocationAddress(address);
                    }
                }

                ((CreateIncidentActivity) getActivity()).showDataFragment();
            }
        });

        MapFragment mapFragment;
        mapFragment = (MapFragment) getChildFragmentManager().findFragmentById(R.id.incident_map);
        mapFragment.getMapAsync(this);

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        initMap();
    }

    private void initMap() {
        LatLng markerLatLng;
        boolean requestPermission = false;

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);

            if (getStoredLatLng() != null) {
                markerLatLng = getStoredLatLng();
            } else {
                LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(new Criteria(), true));

                if (location != null) {
                    markerLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                } else {
                    markerLatLng = getDefaultLatLng();
                }
            }
        } else {
            markerLatLng = getDefaultLatLng();
            requestPermission = true;
        }

        position = markerLatLng;
        map.addMarker(new MarkerOptions().title(getString(R.string.new_inc_marker_title)).draggable(true).position(markerLatLng));
        map.moveCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(markerLatLng, 15)));
        map.getUiSettings().setMapToolbarEnabled(false);
        map.setOnMarkerDragListener(this);

        if (requestPermission) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_REQUEST_LOCATION);
        }

        Toast.makeText(getActivity(), R.string.new_inc_info, Toast.LENGTH_LONG).show();
    }

    private String getAddressFromGeocoder() {
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(position.latitude, position.longitude, 1);
            if (addresses == null || addresses.isEmpty()) {
                return null;
            }

            Address address = addresses.get(0);
            String finalAddress = "";
            if (address.getMaxAddressLineIndex() != -1) {
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    finalAddress += address.getAddressLine(i) + " ";
                }
            }

            return finalAddress;
        } catch (IOException e) {
            Log.e("CreateIncMapFrag", "Error while getting address info.", e);
        }

        return null;
    }

    private LatLng getDefaultLatLng() {
        LatLng storedLatLng = getStoredLatLng();
        return storedLatLng != null ? storedLatLng : new LatLng(48.156541, 17.122091);
    }

    private LatLng getStoredLatLng() {
        AddIncidentDataHolder storage = AddIncidentDataHolder.getInstance();
        if (storage.getLat() == null || storage.getLon() == null) {
            return null;
        } else {
            return new LatLng(storage.getLat(), storage.getLon());
        }
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        // do nothing
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        // do nothing
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        position = marker.getPosition();
    }
}
