/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.SettingsActivity;

public class Utils {

    public static void hideNavigation(Window window) {
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    public static void hideStatusBar(Window window) {
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    public static boolean isDeviceConnected(Context context, boolean showErrorMessage) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        boolean isDeviceConnected = networkInfo != null && networkInfo.isConnected();
        if (!isDeviceConnected && showErrorMessage) {
            Toast.makeText(context, R.string.connection_check_message, Toast.LENGTH_LONG).show();
        }

        return isDeviceConnected;
    }

    public static boolean validateShortEditText(EditText editText, Context context) {
        editText.setError(null);
        String text = editText.getText().toString().trim();

        if (text.length() < 1 || text.length() > 100) {
            editText.setError(context.getString(R.string.validation_short));
            return false;
        }

        return true;
    }

    public static boolean validateMiddleEditText(EditText editText, Context context) {
        editText.setError(null);
        String text = editText.getText().toString().trim();

        if (text.length() < 3 || text.length() > 200) {
            editText.setError(context.getString(R.string.validation_middle));
            return false;
        }

        return true;
    }

    public static boolean validateLongEditText(EditText editText, Context context) {
        editText.setError(null);
        String text = editText.getText().toString().trim();

        if (text.length() < 5 || text.length() > 1000) {
            editText.setError(context.getString(R.string.validation_long));
            return false;
        }

        return true;
    }

    public static String getUsernamePreferenceSetting(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(SettingsActivity.USERNAME_PREFS_KEY, null);
    }

    public static String getEmailPreferenceSetting(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(SettingsActivity.EMAIL_PREFS_KEY, null);
    }

    public static void openProgressDialog(ProgressDialog progressDialog, Context context) {
        if (progressDialog != null) {
            progressDialog.setTitle(context.getString(R.string.progress_dialog_title));
            progressDialog.setMessage(context.getString(R.string.progress_dialog_message));
            progressDialog.show();
        }
    }

    public static void closeProgressDialog(ProgressDialog progressDialog) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
