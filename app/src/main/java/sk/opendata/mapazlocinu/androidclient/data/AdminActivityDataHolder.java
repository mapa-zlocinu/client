/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.data;

import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentAdmin;

public class AdminActivityDataHolder {
    private static AdminActivityDataHolder instance;

    private int adminActivityActiveSection;
    private String adminIncidentDetailId;
    private IncidentAdmin updatedIncident;

    private AdminActivityDataHolder() {
        this.adminActivityActiveSection = 1;
        this.adminIncidentDetailId = "";
    }

    public static AdminActivityDataHolder getInstance() {
        if (instance == null) {
            instance = new AdminActivityDataHolder();
        }

        return instance;
    }

    public int getAdminActivityActiveSection() {
        return adminActivityActiveSection;
    }

    public void setAdminActivityActiveSection(int adminActivityActiveSection) {
        this.adminActivityActiveSection = adminActivityActiveSection;
    }

    public String getAdminIncidentDetailId() {
        return adminIncidentDetailId;
    }

    public void setAdminIncidentDetailId(String adminIncidentDetailId) {
        this.adminIncidentDetailId = adminIncidentDetailId;
    }

    public IncidentAdmin getUpdatedIncident() {
        return updatedIncident;
    }

    public void setUpdatedIncident(IncidentAdmin updatedIncident) {
        this.updatedIncident = updatedIncident;
    }
}
