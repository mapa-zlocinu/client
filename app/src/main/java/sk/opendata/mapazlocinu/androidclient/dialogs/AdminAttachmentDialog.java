/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import sk.opendata.mapazlocinu.androidclient.R;

public class AdminAttachmentDialog extends DialogFragment {
    private static final String ATTACHMENT_ID_KEY = "AdminAttachmentDialog.id";
    private static final String ATTACHMENT_TITLE_KEY = "AdminAttachmentDialog.title";

    public static AdminAttachmentDialog newInstance(String attachmentId, String attachmentTitle) {
        AdminAttachmentDialog dialog = new AdminAttachmentDialog();

        Bundle bundle = new Bundle();
        bundle.putString(ATTACHMENT_ID_KEY, attachmentId);
        bundle.putString(ATTACHMENT_TITLE_KEY, attachmentTitle);
        dialog.setArguments(bundle);

        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final String attachmentId = getArguments().getString(ATTACHMENT_ID_KEY);
        final String attachmentTitle = getArguments().getString(ATTACHMENT_TITLE_KEY);

        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.attachment_dialog_title) + " " + attachmentTitle)
                .setNegativeButton(R.string.about_close, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton(R.string.dialog_delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((AdminAttachmentDialogListener) getTargetFragment()).onAttachmentDeleteRequest(attachmentId);
                        dialogInterface.dismiss();
                    }
                })
                .create();
    }

    public interface AdminAttachmentDialogListener {
        void onAttachmentDeleteRequest(String attachmentId);
    }
}
