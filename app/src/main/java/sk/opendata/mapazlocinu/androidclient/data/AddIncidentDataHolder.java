/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.data;

import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

import sk.opendata.mapazlocinu.androidclient.model.enums.IncidentType;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentCreate;
import sk.opendata.mapazlocinu.androidclient.model.user.UserPublic;

public class AddIncidentDataHolder {
    private static AddIncidentDataHolder instance;

    private String title;
    private String description;
    private Long incidentDate;
    private String username;
    private String userEmail;
    private boolean authorVisible;
    private Double lat;
    private Double lon;
    private String locationAddress;
    private IncidentType type;
    private List<Uri> attachmentsUris;

    private AddIncidentDataHolder() {
        resetData();
    }

    public static AddIncidentDataHolder getInstance() {
        if (instance == null) {
            instance = new AddIncidentDataHolder();
        }

        return instance;
    }

    public void resetData() {
        title = "";
        description = "";
        incidentDate = null;
        username = "";
        userEmail = "";
        authorVisible = true;
        lat = null;
        lon = null;
        locationAddress = "";
        type = null;
        attachmentsUris = new ArrayList<>();
    }

    public IncidentCreate toIncidentCreate() {
        UserPublic userPublic = new UserPublic();
        userPublic.setName(username);
        userPublic.setEmail(userEmail);

        IncidentCreate incidentCreate = new IncidentCreate();
        incidentCreate.setAuthor(userPublic);
        incidentCreate.setTitle(title);
        incidentCreate.setDescription(description);
        incidentCreate.setIncidentDate(incidentDate);
        incidentCreate.setAuthorVisible(authorVisible);
        incidentCreate.setLat(lat);
        incidentCreate.setLon(lon);
        incidentCreate.setLocationAddress(locationAddress);
        incidentCreate.setType(type);

        return incidentCreate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getIncidentDate() {
        return incidentDate;
    }

    public void setIncidentDate(Long incidentDate) {
        this.incidentDate = incidentDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public boolean isAuthorVisible() {
        return authorVisible;
    }

    public void setAuthorVisible(boolean authorVisible) {
        this.authorVisible = authorVisible;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }

    public IncidentType getType() {
        return type;
    }

    public void setType(IncidentType type) {
        this.type = type;
    }

    public List<Uri> getAttachmentsUris() {
        return attachmentsUris;
    }

    public void setAttachmentsUris(List<Uri> attachmentsUris) {
        this.attachmentsUris = attachmentsUris;
    }
}
