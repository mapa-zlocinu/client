/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import java.util.Map;

import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.model.admin.DataSummary;
import sk.opendata.mapazlocinu.androidclient.model.enums.IncidentType;
import sk.opendata.mapazlocinu.androidclient.model.enums.Visibility;
import sk.opendata.mapazlocinu.androidclient.utils.IncidentsUtils;

public class AdminSummaryDialog extends DialogFragment {
    private static final String DATA_KEY = "AdminSummaryDialog.data";

    public static AdminSummaryDialog newInstance(DataSummary dataSummary) {
        AdminSummaryDialog dialog = new AdminSummaryDialog();

        Bundle args = new Bundle();
        args.putSerializable(DATA_KEY, dataSummary);
        dialog.setArguments(args);

        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String textString = "";
        if (getArguments() != null && getArguments().getSerializable(DATA_KEY) != null) {
            textString = createStringDialogMessage((DataSummary) getArguments().getSerializable(DATA_KEY));
        }

        return new AlertDialog.Builder(getActivity())
                .setTitle("Sumár dát")
                .setMessage(textString)
                .setIcon(R.mipmap.ic_launcher)
                .setPositiveButton(R.string.about_close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .create();
    }

    private String createStringDialogMessage(DataSummary dataSummary) {
        StringBuilder builder = new StringBuilder();
        builder.append("Počet incidentov: ").append(dataSummary.getNumberOfIncidents()).append("\n");
        builder.append("Počet komentárov: ").append(dataSummary.getNumberOfComments()).append("\n");
        builder.append("Počet príloh: ").append(dataSummary.getNumberOfAttachments()).append("\n");
        builder.append("Počet používateľov: ").append(dataSummary.getNumberOfUsers()).append("\n\n");

        builder.append("Incidenty podľa viditeľnosti:\n");
        for (Map.Entry<String, Long> entry : dataSummary.getIncidentsByVisibility().entrySet()) {
            builder.append(getString(IncidentsUtils.visibilityToStringResource(Visibility.valueOf(entry.getKey()))))
                    .append(": ").append(entry.getValue()).append("\n");
        }

        builder.append("\n\nIncidenty podľa typu:\n");
        for (Map.Entry<String, Long> entry : dataSummary.getIncidentsByType().entrySet()) {
            builder.append(getString(IncidentsUtils.incidentTypeToStringResource(IncidentType.valueOf(entry.getKey()))))
                    .append(": ").append(entry.getValue()).append("\n");
        }

        builder.append("\n\nKomentáre podľa viditeľnosti:\n");
        for (Map.Entry<String, Long> entry : dataSummary.getCommentsByVisibility().entrySet()) {
            builder.append(getString(IncidentsUtils.visibilityToStringResource(Visibility.valueOf(entry.getKey()))))
                    .append(": ").append(entry.getValue()).append("\n");
        }

        return builder.toString();
    }
}
