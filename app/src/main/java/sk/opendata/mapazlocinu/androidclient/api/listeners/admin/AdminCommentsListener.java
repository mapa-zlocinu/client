/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.api.listeners.admin;

import java.util.List;

import sk.opendata.mapazlocinu.androidclient.model.comment.CommentAdmin;
import sk.opendata.mapazlocinu.androidclient.model.enums.Visibility;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentAdmin;

public interface AdminCommentsListener {
    void onCommentsLoad(List<CommentAdmin> comments, Visibility visibility);

    void onRequestError();

    void onRequestSuccess(CommentAdmin commentAdmin);

    void onRequestSuccess();

    void onCommentIncidentLoad(IncidentAdmin incidentAdmin);
}
