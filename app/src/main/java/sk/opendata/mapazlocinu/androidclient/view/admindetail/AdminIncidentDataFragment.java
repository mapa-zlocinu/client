/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.view.admindetail;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import sk.opendata.mapazlocinu.androidclient.AdminIncidentDetailActivity;
import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.api.ApiAdminController;
import sk.opendata.mapazlocinu.androidclient.api.listeners.admin.AdminIncidentDataListener;
import sk.opendata.mapazlocinu.androidclient.data.AdminActivityDataHolder;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentAdmin;
import sk.opendata.mapazlocinu.androidclient.utils.IncidentsUtils;
import sk.opendata.mapazlocinu.androidclient.utils.Utils;

public class AdminIncidentDataFragment extends Fragment implements AdminIncidentDataListener, OnMapReadyCallback {
    private View view;
    private ProgressDialog progressDialog;

    private GoogleMap map;
    private IncidentAdmin incident;

    public AdminIncidentDataFragment() {
    }

    public static Fragment newInstance() {
        return new AdminIncidentDataFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_admin_incident_data, container, false);

        this.progressDialog = new ProgressDialog(getActivity());
        Utils.openProgressDialog(progressDialog, getActivity());

        MapFragment mapFragment = (MapFragment) getChildFragmentManager().findFragmentById(R.id.incident_map);
        mapFragment.getMapAsync(this);

        String incidentId = ((AdminIncidentDetailActivity) getActivity()).getIncidentId();
        this.incident = ((AdminIncidentDetailActivity) getActivity()).getIncident();
        AdminActivityDataHolder.getInstance().setUpdatedIncident(null);

        ApiAdminController.getInstance().registerListener(this);
        if (this.incident == null) {
            ApiAdminController.getInstance().getIncidentDetail(getActivity(), incidentId);
        } else {
            initView();
            initMap();
            Utils.closeProgressDialog(progressDialog);
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (AdminActivityDataHolder.getInstance().getUpdatedIncident() != null) {
            this.incident = AdminActivityDataHolder.getInstance().getUpdatedIncident();
            ((AdminIncidentDetailActivity) getActivity()).setIncident(this.incident);

            AdminActivityDataHolder.getInstance().setUpdatedIncident(null);
            initView();
        }
    }

    @Override
    public void onIncidentDataLoad(IncidentAdmin incident) {
        this.incident = incident;
        ((AdminIncidentDetailActivity) getActivity()).setIncident(incident);

        initView();
        initMap();
        Utils.closeProgressDialog(progressDialog);
    }

    @Override
    public void onRequestError() {
        Toast.makeText(getActivity(), R.string.admin_data_fail, Toast.LENGTH_LONG).show();
        Utils.closeProgressDialog(progressDialog);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        initMap();
    }

    private void initView() {
        if (this.incident == null) {
            return;
        }

        ((TextView) view.findViewById(R.id.incident_title)).setText(incident.getTitle());
        ((TextView) view.findViewById(R.id.incident_description)).setText(incident.getDescription());

        ((TextView) view.findViewById(R.id.incident_date).findViewById(R.id.text_key)).setText(R.string.incident_date);
        ((TextView) view.findViewById(R.id.incident_date).findViewById(R.id.text_value)).setText(IncidentsUtils.toFormattedDate(incident.getIncidentDate()));

        ((TextView) view.findViewById(R.id.incident_created_date).findViewById(R.id.text_key)).setText(R.string.creation_date);
        ((TextView) view.findViewById(R.id.incident_created_date).findViewById(R.id.text_value)).setText(IncidentsUtils.toFormattedDate(incident.getCreatedDate()));

        ((TextView) view.findViewById(R.id.incident_type).findViewById(R.id.text_key)).setText(R.string.incident_type);
        ((TextView) view.findViewById(R.id.incident_type).findViewById(R.id.text_value)).setText(IncidentsUtils.incidentTypeToStringResource(incident.getType()));

        ((TextView) view.findViewById(R.id.incident_visibility).findViewById(R.id.text_key)).setText(R.string.visibility_title);
        ((TextView) view.findViewById(R.id.incident_visibility).findViewById(R.id.text_value)).setText(IncidentsUtils.visibilityToStringResource(incident.getVisibility()));

        ((TextView) view.findViewById(R.id.incident_author).findViewById(R.id.text_key)).setText(R.string.author);
        if (incident.getAuthor() != null) {
            String author = incident.getAuthor().getName() + "\n" + incident.getAuthor().getEmail();
            if (!incident.isAuthorVisible()) {
                author += "\n" + getString(R.string.comment_anonymous);
            }

            ((TextView) view.findViewById(R.id.incident_author).findViewById(R.id.text_value)).setText(author);
        } else {
            ((TextView) view.findViewById(R.id.incident_author).findViewById(R.id.text_value)).setText(R.string.comment_anonymous);
        }

        ((TextView) view.findViewById(R.id.incident_address).findViewById(R.id.text_key)).setText(R.string.incident_address);
        ((TextView) view.findViewById(R.id.incident_address).findViewById(R.id.text_value)).setText(incident.getLocationAddress());
    }

    private void initMap() {
        if (this.map == null || this.incident == null) {
            return;
        }

        LatLng latLng = IncidentsUtils.incidentExcerptToLatLng(this.incident);
        MarkerOptions options = new MarkerOptions().position(latLng)
                .title(getString(R.string.incident_location))
                .icon(IncidentsUtils.incidentTypeToMarkerColor(this.incident.getType()));

        this.map.getUiSettings().setMapToolbarEnabled(false);
        this.map.getUiSettings().setAllGesturesEnabled(false);
        this.map.getUiSettings().setZoomControlsEnabled(true);
        this.map.addMarker(options);
        this.map.moveCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(latLng, 10)));
    }
}
