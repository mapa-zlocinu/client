/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.dialogs;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;

import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.model.comment.CommentCreate;
import sk.opendata.mapazlocinu.androidclient.model.enums.CommentType;
import sk.opendata.mapazlocinu.androidclient.model.user.UserPublic;
import sk.opendata.mapazlocinu.androidclient.utils.Utils;

public class CreateCommentDialog extends DialogFragment {
    private CreateCommentListener listener;

    @SuppressWarnings("InflateParams")
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final View view = getActivity().getLayoutInflater().inflate(R.layout.comment_create_dialog, null);

        final TextView nameTV = (TextView) view.findViewById(R.id.author_name);
        final TextView emailTV = (TextView) view.findViewById(R.id.author_email);
        String usernamePreference = Utils.getUsernamePreferenceSetting(getActivity());
        String emailPreference = Utils.getEmailPreferenceSetting(getActivity());

        if (usernamePreference != null) nameTV.setText(usernamePreference);
        if (emailPreference != null) emailTV.setText(emailPreference);

        Spinner spinner = (Spinner) view.findViewById(R.id.comment_type);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.comments_type, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        builder.setView(view)
                .setTitle(R.string.new_com_title)
                .setPositiveButton(R.string.new_com_create, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        TextView textTV = (TextView) view.findViewById(R.id.comment_text);

                        String name = nameTV.getText().toString().trim();
                        String email = emailTV.getText().toString().trim();
                        String text = textTV.getText().toString().trim();
                        boolean anonymous = ((CheckBox) view.findViewById(R.id.author_visible)).isChecked();
                        int type = ((Spinner) view.findViewById(R.id.comment_type)).getSelectedItemPosition();

                        UserPublic userPublic = new UserPublic();
                        userPublic.setEmail(email);
                        userPublic.setName(name);

                        CommentCreate commentCreate = new CommentCreate();
                        commentCreate.setAuthor(userPublic);
                        commentCreate.setAuthorVisible(!anonymous);
                        commentCreate.setText(text);

                        if (type == 0) {
                            commentCreate.setType(CommentType.CONFIRMATION);
                        } else if (type == 1) {
                            commentCreate.setType(CommentType.REFUTATION);
                        } else {
                            commentCreate.setType(CommentType.NEUTRAL);
                        }

                        listener.onCreateCommentSubmit(commentCreate);
                    }
                })
                .setNegativeButton(R.string.about_close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (CreateCommentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement CreateCommentListener!");
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (CreateCommentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement CreateCommentListener!");
        }
    }

    public interface CreateCommentListener {
        void onCreateCommentSubmit(CommentCreate commentCreate);
    }
}
