/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import sk.opendata.mapazlocinu.androidclient.api.ApiPublicController;
import sk.opendata.mapazlocinu.androidclient.api.listeners.IncidentAndAttachmentCreateListener;
import sk.opendata.mapazlocinu.androidclient.data.AddIncidentDataHolder;
import sk.opendata.mapazlocinu.androidclient.data.MainActivityDataHolder;
import sk.opendata.mapazlocinu.androidclient.model.CreateItemResponse;
import sk.opendata.mapazlocinu.androidclient.model.enums.Visibility;
import sk.opendata.mapazlocinu.androidclient.model.incident.IncidentPublicExcerpt;
import sk.opendata.mapazlocinu.androidclient.view.create.CreateIncidentDataFragment;
import sk.opendata.mapazlocinu.androidclient.view.create.CreateIncidentMapFragment;


public class CreateIncidentActivity extends AppCompatActivity implements IncidentAndAttachmentCreateListener {
    private static final int MY_PERMISSION_READ_EXTERNAL = 22;
    private static final int READ_REQUEST_CODE = 42;
    private static final String FRAGMENT_DATA_TAG = "add.fragment.data";
    private static final String FRAGMENT_MAP_TAG = "add.fragment.map";

    private FloatingActionButton fab;
    private List<Uri> uploadedAttachments;
    private int successfulUploads;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_incident);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                starFileSearch();
            }
        });

        AddIncidentDataHolder.getInstance().resetData();
        showDataFragment();
    }

    @Override
    public void onBackPressed() {
        if (!fab.isShown()) {
            showDataFragment();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home && !fab.isShown()) {
            showDataFragment();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    public void showDataFragment() {
        Fragment fragment = CreateIncidentDataFragment.newInstance();
        getFragmentManager().beginTransaction()
                .replace(R.id.detail_activity_fragment, fragment, FRAGMENT_DATA_TAG)
                .commit();

        fab.show();
    }

    public void showMapFragment() {
        Fragment fragment = CreateIncidentMapFragment.newInstance();
        getFragmentManager().beginTransaction()
                .replace(R.id.detail_activity_fragment, fragment, FRAGMENT_MAP_TAG)
                .commit();

        fab.hide();
    }

    private void starFileSearch() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            startActivityForResult(intent, READ_REQUEST_CODE);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSION_READ_EXTERNAL);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                Uri uri = data.getData();
                if (uri != null && !AddIncidentDataHolder.getInstance().getAttachmentsUris().contains(uri)) {
                    AddIncidentDataHolder.getInstance().getAttachmentsUris().add(uri);

                    Fragment fragment = getFragmentManager().findFragmentByTag(FRAGMENT_DATA_TAG);
                    if (fragment instanceof CreateIncidentDataFragment) {
                        ((CreateIncidentDataFragment) fragment).reloadAttachmentsNum();
                    }
                }
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case CreateIncidentMapFragment.MY_PERMISSION_REQUEST_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted - reinitialize map?
                } else {
                    // permission denied
                    Toast.makeText(getApplicationContext(), R.string.new_inc_permission_fail, Toast.LENGTH_LONG).show();
                }
            }
            case MY_PERMISSION_READ_EXTERNAL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    starFileSearch();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.new_inc_permission_fail_read, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    public void createIncident() {
        openProgressDialog();
        ApiPublicController.getInstance().registerListener(this);
        ApiPublicController.getInstance().createIncident(AddIncidentDataHolder.getInstance().toIncidentCreate());
    }

    @Override
    public void onIncidentCreateSuccess(CreateItemResponse createItemResponse) {
        this.uploadedAttachments = new ArrayList<>();
        this.successfulUploads = 0;

        if (createItemResponse.getItemStatus().equals(Visibility.APPROVED)) {
            IncidentPublicExcerpt incidentToAdd = new IncidentPublicExcerpt();
            incidentToAdd.setId(createItemResponse.getItemId());
            incidentToAdd.setTitle(AddIncidentDataHolder.getInstance().getTitle());
            incidentToAdd.setType(AddIncidentDataHolder.getInstance().getType());
            incidentToAdd.setLat(AddIncidentDataHolder.getInstance().getLat());
            incidentToAdd.setLon(AddIncidentDataHolder.getInstance().getLon());
            incidentToAdd.setIncidentDate(AddIncidentDataHolder.getInstance().getIncidentDate());

            MainActivityDataHolder.getInstance().getPublicIncidents().add(0, incidentToAdd);
            Toast.makeText(getApplicationContext(), R.string.create_ok_approved, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), R.string.create_ok_pending, Toast.LENGTH_LONG).show();
        }

        uploadAttachments(createItemResponse.getItemId());
    }

    @Override
    public void onIncidentCreateFailure() {
        Toast.makeText(getApplicationContext(), R.string.new_inc_final_fail, Toast.LENGTH_LONG).show();
        closeProgressDialog();
    }

    @Override
    public void onAttachmentCreateSuccess(String incidentId) {
        this.successfulUploads++;
        uploadAttachments(incidentId);
    }

    @Override
    public void onAttachmentCreateFailure(String incidentId) {
        uploadAttachments(incidentId);
    }

    private void uploadAttachments(final String incidentId) {
        if (uploadedAttachments.size() != AddIncidentDataHolder.getInstance().getAttachmentsUris().size()) {
            for (final Uri uri : AddIncidentDataHolder.getInstance().getAttachmentsUris()) {
                if (!uploadedAttachments.contains(uri)) {
                    uploadedAttachments.add(uri);
                    ApiPublicController.getInstance().createAttachment(incidentId, uri, getApplicationContext(), getContentResolver());
                    return;
                }
            }
        } else {
            if (!AddIncidentDataHolder.getInstance().getAttachmentsUris().isEmpty()) {
                Toast.makeText(getApplicationContext(), getString(R.string.new_inc_final_attach, successfulUploads), Toast.LENGTH_LONG).show();
            }

            closeProgressDialog();
            Fragment fragment = getFragmentManager().findFragmentByTag(FRAGMENT_DATA_TAG);
            if (fragment instanceof CreateIncidentDataFragment) {
                ((CreateIncidentDataFragment) fragment).clearData();
            }
        }
    }

    private void openProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(getString(R.string.progress_dialog_title));
        progressDialog.setMessage(getString(R.string.progress_dialog_message));
        progressDialog.show();
    }

    private void closeProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
