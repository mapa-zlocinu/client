/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import sk.opendata.mapazlocinu.androidclient.api.ApiAdminController;
import sk.opendata.mapazlocinu.androidclient.api.listeners.admin.AdminLoginListener;
import sk.opendata.mapazlocinu.androidclient.dialogs.AdminLoginDialog;
import sk.opendata.mapazlocinu.androidclient.model.security.LoginRequest;

public class SettingsActivity extends AppCompatActivity implements AdminLoginDialog.AdminDialogInterface, AdminLoginListener {
    public static final String USERNAME_PREFS_KEY = "cm_user_settings_username";
    public static final String EMAIL_PREFS_KEY = "cm_user_settings_email";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (R.id.action_admin_login == item.getItemId()) {
            new AdminLoginDialog().show(getFragmentManager(), "adminLoginDialog");
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAdminLoginSubmit(String name, String pass) {
        ApiAdminController.getInstance().registerListener(this);
        ApiAdminController.getInstance().adminLogin(getApplicationContext(), new LoginRequest(name, pass));
    }

    @Override
    public void onAdminLoginSuccess() {
        Intent i = new Intent(this, AdminActivity.class);
        startActivity(i);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }

    @Override
    public void onAdminLoginFailure() {
        Toast.makeText(getApplicationContext(), R.string.admin_login_fail, Toast.LENGTH_SHORT).show();
    }
}
