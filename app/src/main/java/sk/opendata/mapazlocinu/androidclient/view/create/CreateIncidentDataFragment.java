/*
 * Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)
 *
 * This file is part of Crime Map - Crowd-sourcing solution (client part).
 *
 * Crime Map - Crowd-sourcing solution (client part) is free software:
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Crime Map - Crowd-sourcing solution (client part) is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Crime Map - Crowd-sourcing solution (client part).
 * If not, see <http://www.gnu.org/licenses/>.
 */

package sk.opendata.mapazlocinu.androidclient.view.create;


import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import sk.opendata.mapazlocinu.androidclient.CreateIncidentActivity;
import sk.opendata.mapazlocinu.androidclient.R;
import sk.opendata.mapazlocinu.androidclient.data.AddIncidentDataHolder;
import sk.opendata.mapazlocinu.androidclient.model.enums.IncidentType;
import sk.opendata.mapazlocinu.androidclient.utils.IncidentsUtils;
import sk.opendata.mapazlocinu.androidclient.utils.Utils;

public class CreateIncidentDataFragment extends Fragment implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {
    private View view;

    private EditText incTitleEditText;
    private EditText incDescEditText;
    private EditText incLocationEditText;
    private EditText authorNameEditText;
    private EditText authorEmailEditText;
    private CheckBox authorAnonymCheckBox;
    private Spinner incTypeSpinner;
    private Button incDateTimeButton;
    private LinearLayout attachmentLayout;
    private TextView attachmentNumTextView;

    private boolean wasTimeSelected = false;
    private Calendar selectedDateTime;

    public CreateIncidentDataFragment() {
    }

    public static Fragment newInstance() {
        return new CreateIncidentDataFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_incident_data, container, false);
        initView();

        return view;
    }

    public void reloadAttachmentsNum() {
        AddIncidentDataHolder storage = AddIncidentDataHolder.getInstance();

        if (storage.getAttachmentsUris().isEmpty()) {
            attachmentLayout.setVisibility(View.GONE);
        } else {
            attachmentLayout.setVisibility(View.VISIBLE);
            attachmentNumTextView.setText(getString(R.string.new_inc_attach_num, storage.getAttachmentsUris().size()));
        }
    }

    public void clearData() {
        AddIncidentDataHolder.getInstance().resetData();
        loadStoredInputs();
    }

    private void initView() {
        incTypeSpinner = (Spinner) view.findViewById(R.id.inc_type);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.incident_type, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        incTypeSpinner.setAdapter(adapter);

        incTitleEditText = (EditText) view.findViewById(R.id.inc_title);
        incDescEditText = (EditText) view.findViewById(R.id.inc_description);
        incLocationEditText = (EditText) view.findViewById(R.id.inc_location);
        authorNameEditText = (EditText) view.findViewById(R.id.inc_author_name);
        authorEmailEditText = (EditText) view.findViewById(R.id.inc_author_email);
        authorAnonymCheckBox = (CheckBox) view.findViewById(R.id.inc_author_visible);
        attachmentLayout = (LinearLayout) view.findViewById(R.id.attachment_layout);
        attachmentNumTextView = (TextView) view.findViewById(R.id.inc_attachment_num);

        incDateTimeButton = (Button) view.findViewById(R.id.inc_date_time);
        incDateTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog();
            }
        });

        view.findViewById(R.id.inc_map_picker).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                storeInputs();
                ((CreateIncidentActivity) getActivity()).showMapFragment();
            }
        });

        view.findViewById(R.id.inc_attachment_del).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddIncidentDataHolder.getInstance().setAttachmentsUris(new ArrayList<Uri>());
                attachmentLayout.setVisibility(View.GONE);
            }
        });

        view.findViewById(R.id.inc_create).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createIncident();
            }
        });

        loadStoredInputs();

        String usernamePrefs = Utils.getUsernamePreferenceSetting(getActivity());
        String emailPrefs = Utils.getEmailPreferenceSetting(getActivity());
        if (usernamePrefs != null) {
            authorNameEditText.setText(usernamePrefs);
        }
        if (emailPrefs != null) {
            authorEmailEditText.setText(emailPrefs);
        }
    }

    private void loadStoredInputs() {
        AddIncidentDataHolder storage = AddIncidentDataHolder.getInstance();

        incTitleEditText.setText(storage.getTitle());
        incDescEditText.setText(storage.getDescription());
        incLocationEditText.setText(storage.getLocationAddress());
        authorNameEditText.setText(storage.getUsername());
        authorEmailEditText.setText(storage.getUserEmail());
        authorAnonymCheckBox.setChecked(!storage.isAuthorVisible());

        if (storage.getType() != null) {
            incTypeSpinner.setSelection(IncidentsUtils.incidentTypeToSpinnerIndex(storage.getType()));
        }
        if (storage.getIncidentDate() != null) {
            wasTimeSelected = true;
            selectedDateTime = Calendar.getInstance();
            selectedDateTime.setTime(new Date(storage.getIncidentDate()));
            incDateTimeButton.setText(IncidentsUtils.toFormattedDate(storage.getIncidentDate()));
        } else {
            wasTimeSelected = false;
            incDateTimeButton.setText(R.string.new_inc_time);
        }

        if (storage.getAttachmentsUris().isEmpty()) {
            attachmentLayout.setVisibility(View.GONE);
        } else {
            attachmentLayout.setVisibility(View.VISIBLE);
            attachmentNumTextView.setText(getString(R.string.new_inc_attach_num, storage.getAttachmentsUris().size()));
        }
    }

    private void storeInputs() {
        AddIncidentDataHolder storage = AddIncidentDataHolder.getInstance();

        storage.setTitle(incTitleEditText.getText().toString());
        storage.setDescription(incDescEditText.getText().toString());
        storage.setUsername(authorNameEditText.getText().toString());
        storage.setUserEmail(authorEmailEditText.getText().toString());
        storage.setAuthorVisible(!authorAnonymCheckBox.isChecked());
        storage.setLocationAddress(incLocationEditText.getText().toString());
        storage.setType(spinnerToIncidentType());

        if (wasTimeSelected && selectedDateTime != null) {
            storage.setIncidentDate(selectedDateTime.getTimeInMillis());
        }
    }

    private void showDateDialog() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dateDialog = DatePickerDialog.newInstance(this, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
        dateDialog.setMaxDate(now);
        dateDialog.dismissOnPause(true);
        dateDialog.show(getFragmentManager(), "DatePickerDialog");
    }

    private void showTimeDialog() {
        Calendar now = Calendar.getInstance();
        TimePickerDialog dateDialog = TimePickerDialog.newInstance(this, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), true);
        dateDialog.dismissOnPause(true);
        dateDialog.show(getFragmentManager(), "TimePickerDialog");
    }

    private void createIncident() {
        boolean isError = false;

        if (!Utils.validateMiddleEditText(incTitleEditText, getActivity()) || !Utils.validateLongEditText(incDescEditText, getActivity()) ||
                !Utils.validateMiddleEditText(incLocationEditText, getActivity()) || !Utils.validateShortEditText(authorNameEditText, getActivity()) ||
                !Utils.validateShortEditText(authorEmailEditText, getActivity())) {
            isError = true;
        }

        if (!wasTimeSelected || selectedDateTime == null || selectedDateTime.after(Calendar.getInstance())) {
            isError = true;
            Toast.makeText(getActivity(), R.string.new_inc_err_date, Toast.LENGTH_SHORT).show();
        }

        if (AddIncidentDataHolder.getInstance().getLat() == null || AddIncidentDataHolder.getInstance().getLon() == null) {
            isError = true;
            Toast.makeText(getActivity(), R.string.new_inc_err_map, Toast.LENGTH_SHORT).show();
        }

        if (!isError) {
            storeInputs();
            ((CreateIncidentActivity) getActivity()).createIncident();
        }
    }

    private IncidentType spinnerToIncidentType() {
        return IncidentType.values()[incTypeSpinner.getSelectedItemPosition()];
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        wasTimeSelected = false;
        selectedDateTime = Calendar.getInstance();
        selectedDateTime.set(year, monthOfYear, dayOfMonth);
        showTimeDialog();
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        wasTimeSelected = true;
        selectedDateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        selectedDateTime.set(Calendar.MINUTE, minute);
        selectedDateTime.set(Calendar.SECOND, second);

        incDateTimeButton.setText(IncidentsUtils.toFormattedDate(selectedDateTime.getTimeInMillis()));
    }
}
