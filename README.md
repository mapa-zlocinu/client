﻿# Mapa Zločinu (Crime Map) Crowd-sourcing solution (Client part)

This repository contains client part of application used as Crowd-sourcing solution for project
[Crime map](http://mapazlocinu.opendata.sk/). It is the application with focus on collecting information
about various offenses and crimes from people. This project is an Android application which uses API 
of server part of this solution to provide all features to users.

## Compilation and run
This project is standard Android project, so the best approach is to open and compile this project 
in Android Studio (or alternatively you can build this project directly with Gradle).

### Settings
There are two configuration settings you have to do before running the application:
1. Change the value of **BASE_URL** constant in the file _sk.opendata.mapazlocinu.androidclient.api.config.ApiClient.java_ - it is the URL of server providing API
2. Add **Google Maps API key** to the file _google_maps_api.xml_ in the _app\src\main\res\values_ folder

## Licence
Copyright 2017 Project Crime Map - Crowd-sourcing solution (http://mapazlocinu.opendata.sk/)

This file is part of Crime Map - Crowd-sourcing solution (client part).

Crime Map - Crowd-sourcing solution (client part) is free software:
you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

Crime Map - Crowd-sourcing solution (client part) is distributed
in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Crime Map - Crowd-sourcing solution (client part).
If not, see <http://www.gnu.org/licenses/>.


This project uses following 3rd party components and libraries:
- Retrofit [Apache 2.0 license]
- Google Maps [Standard Plan: For free and publicly available apps/websites]
- ZGallery [Apache 2.0 license]
- MaterialDateTimePicker [Apache 2.0 license]
- FabOptions [Apache 2.0 license]
- Picasso [Apache 2.0 license]
- Picasso2 - Okhttp3Downloader [Apache 2.0 license]
- Android Image Popup [MIT]